#include "UnitManager.h"

using namespace MyBot;

UnitManager & UnitManager::Instance()
{
	static UnitManager instance;
	return instance;
}

UnitManager::UnitManager()
{
	observerBattle = nullptr;
	observerScout = nullptr;
	observerDeffence = nullptr;
	mainCombatUnitSet = std::set<BWAPI::Unit>();
	deffenceUnitSet = std::set<BWAPI::Unit>();
	genseiUnitSet = std::set<BWAPI::Unit>();
}

void UnitManager::update()
{
	//각 종족별 기본 전략 전술 코딩
	// executeRaceBuildStrategy : 건물/테크/병력생산 등
	// executeRaceUnitStrategy : 유닛의 움직임 컨트롤 = 유닛별 행동강령
	if (BWAPI::Broodwar->self()->getRace() == BWAPI::Races::Zerg){
		executeZergUnitStrategy();
	}

	if (BWAPI::Broodwar->self()->getRace() == BWAPI::Races::Protoss){
		executeProtossUnitStrategy();
	}

	if (BWAPI::Broodwar->self()->getRace() == BWAPI::Races::Terran){
		executeTerranUnitStrategy();
	}

}


void UnitManager::executeTerranUnitStrategy(){
	return;
}


void UnitManager::executeProtossUnitStrategy(){
	////////////////////////////////////////////////////////// 아칸합체
	for (auto & unit : BWAPI::Broodwar->self()->getUnits())
	{
		// 건물은 제외
		if (unit->getType().isBuilding()) continue;
		// 모든 일꾼은 제외
		if (unit->getType().isWorker()) continue;

		//질럿

		//드라군
		// 드라군점사, 탱크점사 & 사거리 짧은유닛 대상 치고빠지기 등

		//하이템플러 (일단은 아칸만 운용 추후 필요시 스톰 로직)

		//다크템플러
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Dark_Templar && unit->isCompleted() ){
			if (unit->isIdle() && unit->canAttackMove()){
				std::vector<BWTA::BaseLocation *> neutralBaseLocations = InformationManager::Instance().getNeutralBaseLocations();
				if (neutralBaseLocations.size() != 0){
					BWTA::BaseLocation * targetLocation = nullptr;
					//std::cout << rand();
					int random = rand() % neutralBaseLocations.size();
					/*printf("다크야다크야 모하니" << neutralBaseLocations.size();
					if (neutralBaseLocations.size() != 0 && neutralBaseLocations.back() != nullptr){
						printf("111";
						std::cout << neutralBaseLocations.back()->getPosition();
						printf("2222";
						CommandUtil::attackMove(unit, neutralBaseLocations.back()->getPosition());
					}*/

					targetLocation = neutralBaseLocations.at(random);
					if (targetLocation){
						CommandUtil::attackMove(unit, targetLocation->getPosition());
					}
				}
				else{

				}
			}
		}

		//아칸

		//다크아칸(미운용예정)

		//스카웃

		//커세어

		//캐리어

		//아비터

		//셔틀

		//리버

		//옵저버
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Observer && unit->isCompleted()){
			// 각 특수목적 옵저버가 없으면 지정, 그외 옵저버들 움직임
			if (observerBattle == nullptr){
				observerBattle = unit;
			}
			else if (observerScout == nullptr){
				observerScout = unit;
			}
			else if (observerDeffence == nullptr){
				observerDeffence = unit;
			}
			else{
				// TODO : 상대 기지가 중립기지와 중립기지의 중간에 있을때 그곳은 피해가도록 설정해야함
				// (현재는 헌터에서 1시와 5시가 중립기지일 경우 3시에 적기지가 있으면 가서 자살함)
				if (unit->isIdle() && unit->canMove()){
					std::vector<BWTA::BaseLocation *> neutralBaseLocations = InformationManager::Instance().getNeutralBaseLocations();
					if (neutralBaseLocations.size()!=0){
						BWTA::BaseLocation * targetLocation = nullptr;
						//std::cout << rand();
						int random = rand() % neutralBaseLocations.size();
						//for (BWTA::BaseLocation * baseLocation : neutralBaseLocations){
						//	if (baseLocation->getTilePosition() != unit->getTilePosition()){
						//		targetLocation = baseLocation;
						//		std::cout << baseLocation->getPosition() << "정찰고고"<<std::endl;
						//		break;
						//	}
						//}
						targetLocation = neutralBaseLocations.at(random);
						if (targetLocation){
							CommandUtil::move(unit, targetLocation->getPosition());
						}
					} else{
					}
				}
			}

			// observerBattle 전투옵저버 움직임
			if (observerBattle){

			}

			// observerBattle 정철옵저버 움직임
			if (observerScout){

			}

			// observerBattle 방어옵저버 움직임
			if (observerDeffence){

			}

		}























		//하이템플러 합체
		if (unit->getType() == BWAPI::UnitTypes::Protoss_High_Templar && unit->isCompleted() && unit->isIdle()){
			if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Archon) == 0) {
				BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Archon);
				break;
			}
		}
	}
}



void UnitManager::executeZergUnitStrategy(){
	return;
}