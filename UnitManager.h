#pragma once

#include "Common.h"

#include "UnitData.h"
#include "BuildOrderQueue.h"
#include "InformationManager.h"
#include "WorkerManager.h"
#include "BuildManager.h"
#include "ConstructionManager.h"
#include "ScoutManager.h"
#include "StrategyManager.h"
#include "UnitManager.h"

namespace MyBot
{
	/// 각 유닛의 전술적인 움직임이 없을 경우
	/// 각 유닛에 특수한 임무를 부여한 경우 (다크견제, 옵저버정찰 등)
	class UnitManager
	{
		UnitManager();

		// 자신의 유닛의 개수파악 my
		int mProbe, mZeolot, mDragoon, mHighTemplar, mArchon, mDarkTemplar, mDarkArchon, mShuttle, mReaver, mOvserver, mScout, mCorsair, mCarrier, mArbiter;

		// 적(프로토스) 유닛의 개수파악 enemy
		int eProbe, eZeolot, eDragoon, eHighTemplar, eArchon, eDarkTemplar, eDarkArchon, eShuttle, eReaver, eOvserver, eScout, eCorsair, eCarrier, eArbiter;

		// 적(테란) 유닛의 현재 개수파악
		// int nProbe, nZeolot, nDragoon, nHighTemplar, nArchon, nDarkTemplar, nDarkArchon, nShuttle, nReaver, nOvserver, nScout, nCorsair, nCarrier, nArbiter;

		// 적(저그) 유닛의 현재 개수파악
		// int nProbe, nZeolot, nDragoon, nHighTemplar, nArchon, nDarkTemplar, nDarkArchon, nShuttle, nReaver, nOvserver, nScout, nCorsair, nCarrier, nArbiter;

		// 기본 전투병력구성
		// 지상
		// 질럿 + 드라군 (+ 하이템플러 + 아칸 + 다크템플러 + 아비터)
		// 공중
		// 캐리어 + 아비터 (+ 셔틀템플러 + 커세어)


		// 병력은 각각의 목적을 가진 유닛들의 집합들로 이루어야할듯 maybe
		// ex) 본진방어용Set, 센터장악용Set, 견제용Set, 멀티자르기용Set, 방어용Set 등
		// 으로 이루어져 있다가 공격타이밍이나 적공격타이밍에는 대부분 공격용전투Set 혹은 방어용전투Set 으로 전환한다던가
		// 공격&센터장악
		std::set <BWAPI::Unit> mainCombatUnitSet;
		// 방어
		std::set <BWAPI::Unit> deffenceUnitSet;
		// 겐세이
		std::set <BWAPI::Unit> genseiUnitSet;


		// 특수활동필요유닛
		// 아군멀티시도지역 정찰용 질드라다크
		// 견제 난입용 다크
		// 리콜용아비터
		// 멀티셔틀 드랍셔틀
		// 멀티방어용하이템플러
		/// 옵저버 중 한명을 전투용으로 정해서 메인전투부대와 같이 움직입니다
		BWAPI::Unit observerBattle;
		/// 옵저버 중 한명을 정찰용으로 정해서 목숨걸고 상대기지를 정찰합니다
		BWAPI::Unit observerScout;
		/// 옵저버 중 한명을 방어용으로 정해서 다크난입이나 드랍 등을 방어합니다
		BWAPI::Unit observerDeffence;

		///////////TODO : 일꾼들 테러 안당하도록 회피하는 로직 필요

		// 각 종족의 유닛별 기본 전술(unit 각개 움직임) 코딩
		// 기본적으로 각 Set에는 포함되어있지만 일반적인 전투시의 움직임
		// 및 특수활동시 움직임 코딩필요
		void executeZergUnitStrategy();
		void executeProtossUnitStrategy();
		void executeTerranUnitStrategy();


	public:

		/// static singleton 객체를 리턴합니다
		static UnitManager &	Instance();

		/// 경기 진행 중 매 프레임마다 경기 전략 관련 로직을 실행합니다
		void update();
	};
}
