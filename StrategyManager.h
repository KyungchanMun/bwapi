#pragma once

#include "Common.h"

#include "UnitData.h"
#include "BuildOrderQueue.h"
#include "InformationManager.h"
#include "WorkerManager.h"
#include "BuildManager.h"
#include "ConstructionManager.h"
#include "ScoutManager.h"
#include "StrategyManager.h"
#include "UnitManager.h"

namespace MyBot
{
	/// 상황을 판단하여, 정찰, 빌드, 공격, 방어 등을 수행하도록 총괄 지휘를 하는 class
	/// InformationManager 에 있는 정보들로부터 상황을 판단하고, 
	/// BuildManager 의 buildQueue에 빌드 (건물 건설 / 유닛 훈련 / 테크 리서치 / 업그레이드) 명령을 입력합니다.
	/// 정찰, 빌드, 공격, 방어 등을 수행하는 코드가 들어가는 class
	class StrategyManager
	{
		StrategyManager();
		bool isInitialBuildOrderFinished;
		//TODO : 초기빌드오더도 상대 빌드를 보아서 대응할 수 있도록 해야함 반드시
		// 정찰유무에 따른 것도 구현필요



		//토스 테크건물 착공(트라이)여부
		bool isCyberneticsCore = false, isCitadelOfAdun = false, isForge = false, isTemplarArchives = false, isStargate = false, isProtossArbiterTribunal = false, isRoboticsFacility = false, isObservatory = false;
		//토스 테크건물 현재완성여부
		bool isCyberneticsCoreComplete = false, isCitadelOfAdunComplete = false, isForgeComplete = false, isTemplarArchivesComplete = false, isStargateComplete = false, isArbiterTribunalComplete = false, isRoboticsFacilityComplete = false, isObservatoryComplete = false;


		void setInitialBuildOrder();

		// 일꾼 생산 관리
		void executeWorkerTraining();

		// 파일런(인구) 관리
		void executeSupplyManagement();

		// 공격을 할것이냐 말것이냐 판단, 
		//TODO 공격할건지 판단하고 어디로 어떤병력을 얼마나 할애할거냐도 판단해야함 현재는 총공격모드 only 
		bool isFullScaleAttackStarted;
		void executeCombat();

		// 확장(멀티)
		void executeAnotherExtension();

		// 테크건물 파괴당할시 복구로직
		void executeReconstruction();
		 
		//각 종족별 기본 전략(build 생산 멀티 등) 전술(unit 역할)
		void executeProtossVsZergBuildStrategy();
		void executeProtossVsTerranBuildStrategy();
		void executeProtossVsProtossBuildStrategy();

		//일단 토스 구현
		//void executeZergBuildStrategy();
		//void executeTerranBuildStrategy();

		///////// 유닛별 움직임 (전술) => UnitManager로 뺌
		//void executeProtossUnitStrategy();
		//void executeZergUnitStrategy();
		//void executeTerranUnitStrategy();


		///////////////////////////////////////////////////////////////////////////////////////////
		//프저전
		void buildVsZergArchonFullAttackWithoutStargate();

		void buildVsZergWeaponLegUpgradeZealot();

		///////////////////////////////////////////////////////////////////////////////////////////
		//프테전
		//상대가 테란이고 로템일경우
		void buildVsTerranFastDarkTemplar();

		void buildVsTerranDoubleNexus();

		///////////////////////////////////////////////////////////////////////////////////////////
		//프프전 빌드 경우의수
		//첫서치일경우 가스러쉬 할것!!!
		//상대노가스 -> 질럿올인 -> 경찬몬 드라군컨트롤로 질럿 다 잡아낼 수 있음
		//상대코어완성후 사업 -> 더블넥/옵드라/리버/4겟올인
		//상대사업안하고 아둔 -> 다크
		//다크더블은 템플러아카이브와 두번째게이트가 동시에 올라가고 2다크찍고 멀티하는빌드임

		//정찰불가시, 상대선아둔시, 사업타이밍인데 노사업시
		void buildVsProtossObDra3Gate();

		//상대 노로보틱스로 3겟이상시
		void buildVsProtossFastDarkTemplar();

		//상대 1게이트 사업로보틱스시 -> 앞마당 넥서스 소환하고보자, 드라군 안쉬면서 3게이트
		void buildVsProtossDoubleNexus();
		///////////////////////////////////////////////////////////////////////////////////////////
		//상대가 랜덤일 경우
		void buildVsRandom();
		///////////////////////////////////////////////////////////////////////////////////////////


	public:
		/// static singleton 객체를 리턴합니다
		static StrategyManager &	Instance();

		/// 경기가 시작될 때 일회적으로 전략 초기 세팅 관련 로직을 실행합니다
		void onStart();

		///  경기가 종료될 때 일회적으로 전략 결과 정리 관련 로직을 실행합니다
		void onEnd(bool isWinner);

		/// 경기 진행 중 매 프레임마다 경기 전략 관련 로직을 실행합니다
		void update();
	};
}
