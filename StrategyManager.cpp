#include "StrategyManager.h"

using namespace MyBot;

StrategyManager & StrategyManager::Instance()
{
	static StrategyManager instance;
	return instance;
}

StrategyManager::StrategyManager()
{
	isFullScaleAttackStarted = false;
	isInitialBuildOrderFinished = false;
}

void StrategyManager::onStart()
{
	setInitialBuildOrder();
}

void StrategyManager::onEnd(bool isWinner)
{	
}

void StrategyManager::update()
{
	// 상대 종족에 따른 초기 빌드오더
	if (BuildManager::Instance().buildQueue.isEmpty()) {
		isInitialBuildOrderFinished = true;
	}

	// 일꾼 생산 관리
	executeWorkerTraining();

	// 파일런(인구) 관리
	executeSupplyManagement();

	// 공격을 할것이냐 말것이냐 판단, 
	//TODO 공격할건지 판단하고 어디로 어떤병력을 얼마나 할애할거냐도 판단해야함 현재는 총공격모드 only 
	executeCombat();

	// 유닛컨트롤
	UnitManager::Instance().update();

	// 확장(멀티)
	executeAnotherExtension();

	// 건물 파괴당할시 복구로직
	executeReconstruction();

	if (BWAPI::Broodwar->getFrameCount() % 24 != 0) {
		return;
	}

	// 프로토스 각 종족별 상대할 때 빌드전략
	if (BWAPI::Broodwar->self()->getRace() == BWAPI::Races::Protoss){
		if (BWAPI::Broodwar->enemy()->getRace() == BWAPI::Races::Protoss){
			// P vs P
			executeProtossVsProtossBuildStrategy();
		}
		if (BWAPI::Broodwar->enemy()->getRace() == BWAPI::Races::Terran){
			// P vs T
			executeProtossVsTerranBuildStrategy();
		}
		if (BWAPI::Broodwar->enemy()->getRace() == BWAPI::Races::Zerg){
			// P vs Z
			executeProtossVsZergBuildStrategy();
		}
	}

	//TODO : 메소드 분리 필요
	//각 종족별 기본 전략 전술 코딩
	// executeRaceBuildStrategy : 건물/테크/병력생산 등
	// executeRaceUnitStrategy : 유닛의 움직임 컨트롤 = 유닛별 행동강령
	//if (BWAPI::Broodwar->self()->getRace() == BWAPI::Races::Zerg){
	//	executeZergBuildStrategy();
	//	//executeZergUnitStrategy();
	//}

	//if (BWAPI::Broodwar->self()->getRace() == BWAPI::Races::Terran){
	//	executeTerranBuildStrategy();
	//	//executeTerranUnitStrategy();
	//}

}

void StrategyManager::setInitialBuildOrder()
{
	if (BWAPI::Broodwar->self()->getRace() == BWAPI::Races::Protoss) {

		//vs 저그 노스타빌드(더블넥->드라군->질럿+아칸(+옵저버))

		//패스트다크 (8파일런9게이트11가스12코어13질럿17아둔17전진파일런17전진투게이트17템플러아카이브19.3다크25파일런) ===> 상대기지 다크도착시간 : 게임시간기준5분
		//vs 테란 (더블넥 기본->옵저버->(템플러)->아비터)

		//맵이 헌터이고 더블커맨드 확인하고 헌터일경우 언덕이 없으므로 질드라 어택땅
		//드라군한부대 + 옵저버 + 나머지 다 질럿
		//vs 테란 11분200뚫기빌드 (8파일런 10게이트 11가스 13코어 15파일런 17드라군 19사업 29넥서스 33로보)
		// 3드라군되면 하나 입구막고 2드라군 상대앞마당 체크 -> 앞마당 더블커맨드 확인하면 바로 트리플
		// 중간에 발업 3룡이 가스 안캐고 10게이트에서 드라군한부대만 유지 나머지 질럿

		//vs 토스 (질럿올인(노가스) -> 2게이트 따라가면서 그냥드라군)
		//vs 토스 (4게이트 질럿드라군 올인 -> 다크)
		//vs 토스 (리버/옵드라 등 선로보틱스 -> 2게이트 앞마당 후 6게이트)


		//vs 랜덤 (본진플레이 기본 -> 정찰후 종족발견 후에 결정)
		// 랜덤이면 반드시 파일런서치, 저그여도 파일런서치, 테란토스면 게이트서치하면될듯

		if (BWAPI::Broodwar->enemy()->getRace() == BWAPI::Races::Protoss){
			printf("상대가 토스구나\n");
			//본진플레이가 정석
			buildVsProtossObDra3Gate();
		}
		else if (BWAPI::Broodwar->enemy()->getRace() == BWAPI::Races::Terran){
			printf("상대가 테란구나\n");
			//더블넥 정석
			buildVsTerranDoubleNexus();
		}
		else if (BWAPI::Broodwar->enemy()->getRace() == BWAPI::Races::Zerg){
			printf("상대가 저그구나\n");
			//더블넥 정석
			buildVsZergArchonFullAttackWithoutStargate();
		}
		else{
			printf("상대가 이도저도아니구나\n");
			buildVsRandom();
		}
	}
}

// 일꾼 계속 추가 생산
void StrategyManager::executeWorkerTraining()
{
	// InitialBuildOrder 진행중에는 아무것도 하지 않습니다
	if (isInitialBuildOrderFinished == false) {
		return;
	}
	if (BWAPI::Broodwar->self()->getRace() == BWAPI::Races::Zerg) {
		return;
	}

	if (BWAPI::Broodwar->self()->minerals() >= 50) {
		// workerCount = 현재 일꾼 수 + 생산중인 일꾼 수
		int workerCount = BWAPI::Broodwar->self()->allUnitCount(InformationManager::Instance().getWorkerType());

		if (BWAPI::Broodwar->self()->getRace() == BWAPI::Races::Zerg) {

			for (auto & unit : BWAPI::Broodwar->self()->getUnits())
			{
				if (unit->getType() == BWAPI::UnitTypes::Zerg_Egg) {
					// Zerg_Egg 에게 morph 명령을 내리면 isMorphing = true, isBeingConstructed = true, isConstructing = true 가 된다
					// Zerg_Egg 가 다른 유닛으로 바뀌면서 새로 만들어진 유닛은 잠시 isBeingConstructed = true, isConstructing = true 가 되었다가, 
					if (unit->isMorphing() && unit->getBuildType() == BWAPI::UnitTypes::Zerg_Drone) {
						workerCount++;
					}
				}
			}
		}
		else {
			for (auto & unit : BWAPI::Broodwar->self()->getUnits())
			{
				if (unit->getType().isResourceDepot())
				{
					if (unit->isTraining()) {
						workerCount += unit->getTrainingQueue().size();
					}
				}
			}
		}

		int nNexus = InformationManager::Instance().getNumUnits(InformationManager::Instance().getBasicResourceDepotBuildingType(), InformationManager::Instance().selfPlayer);

		if (workerCount < (25 * nNexus > 60 ? 60 : 25 * nNexus)) {
			for (auto & unit : BWAPI::Broodwar->self()->getUnits())
			{
				if (unit->getType().isResourceDepot())
				{
					if (unit->isTraining() == false || unit->getLarva().size() > 0) {

						// 빌드큐에 일꾼 생산이 1개는 있도록 한다
						if (BuildManager::Instance().buildQueue.getItemCount(InformationManager::Instance().getWorkerType()) == 0) {
							//std::cout << "worker enqueue" << std::endl;
							BuildManager::Instance().buildQueue.queueAsLowestPriority(MetaType(InformationManager::Instance().getWorkerType()), false);
						}
					}
				}
			}
		}
	}
}

// Supply DeadLock 예방 및 SupplyProvider 가 부족해질 상황 에 대한 선제적 대응으로서 SupplyProvider를 추가 건설/생산한다
void StrategyManager::executeSupplyManagement()
{
	// InitialBuildOrder 진행중에는 아무것도 하지 않습니다
	if (isInitialBuildOrderFinished == false) {
		return;
	}

	// 1초에 한번만 실행
	if (BWAPI::Broodwar->getFrameCount() % 24 != 0) {
		return;
	}

	// 게임에서는 서플라이 값이 200까지 있지만, BWAPI 에서는 서플라이 값이 400까지 있다
	// 저글링 1마리가 게임에서는 서플라이를 0.5 차지하지만, BWAPI 에서는 서플라이를 1 차지한다
	if (BWAPI::Broodwar->self()->supplyTotal() < 400)
	{
		// 서플라이가 다 꽉찼을때 새 서플라이를 지으면 지연이 많이 일어나므로, supplyMargin (게임에서의 서플라이 마진 값의 2배)만큼 부족해지면 새 서플라이를 짓도록 한다
		// 이렇게 값을 정해놓으면, 게임 초반부에는 서플라이를 너무 일찍 짓고, 게임 후반부에는 서플라이를 너무 늦게 짓게 된다
		// int supplyMargin = 4 * InformationManager::Instance().getNumUnits(InformationManager::Instance().getBasicCombatBuildingType(), BWAPI::Broodwar->self());

		int supplyMargin = 0;

		if (BWAPI::Broodwar->self()->getRace() == BWAPI::Races::Zerg){
			supplyMargin = 2 * InformationManager::Instance().getNumUnits(InformationManager::Instance().getBasicCombatBuildingType(), BWAPI::Broodwar->self());
		}
		else{
			supplyMargin = 6 * InformationManager::Instance().getNumUnits(InformationManager::Instance().getBasicCombatBuildingType(), BWAPI::Broodwar->self());
		}

		// currentSupplyShortage 를 계산한다
		int currentSupplyShortage = BWAPI::Broodwar->self()->supplyUsed() + supplyMargin - BWAPI::Broodwar->self()->supplyTotal();

		if (currentSupplyShortage > 0) {

			// 생산/건설 중인 Supply를 센다
			int onBuildingSupplyCount = 0;

			// 저그 종족인 경우, 생산중인 Zerg_Overlord (Zerg_Egg) 를 센다. Hatchery 등 건물은 세지 않는다
			if (BWAPI::Broodwar->self()->getRace() == BWAPI::Races::Zerg) {
				for (auto & unit : BWAPI::Broodwar->self()->getUnits())
				{
					if (unit->getType() == BWAPI::UnitTypes::Zerg_Egg && unit->getBuildType() == BWAPI::UnitTypes::Zerg_Overlord) {
						onBuildingSupplyCount += BWAPI::UnitTypes::Zerg_Overlord.supplyProvided();
					}
					// 갓태어난 Overlord 는 아직 SupplyTotal 에 반영안되어서, 추가 카운트를 해줘야함 
					if (unit->getType() == BWAPI::UnitTypes::Zerg_Overlord && unit->isConstructing()) {
						onBuildingSupplyCount += BWAPI::UnitTypes::Zerg_Overlord.supplyProvided();
					}
				}
			}
			// 저그 종족이 아닌 경우, 건설중인 Protoss_Pylon, Terran_Supply_Depot 를 센다. Nexus, Command Center 등 건물은 세지 않는다
			else {
				onBuildingSupplyCount += ConstructionManager::Instance().getConstructionQueueItemCount(InformationManager::Instance().getBasicSupplyProviderUnitType()) * InformationManager::Instance().getBasicSupplyProviderUnitType().supplyProvided();
			}

			std::cout << "currentSupplyShortage : " << currentSupplyShortage << " onBuildingSupplyCount : " << onBuildingSupplyCount << std::endl;

			if (currentSupplyShortage > onBuildingSupplyCount) {

				// BuildQueue 최상단에 SupplyProvider 가 있지 않으면 enqueue 한다
				bool isToEnqueue = true;
				if (!BuildManager::Instance().buildQueue.isEmpty()) {
					BuildOrderItem currentItem = BuildManager::Instance().buildQueue.getHighestPriorityItem();
					if (currentItem.metaType.isUnit() && currentItem.metaType.getUnitType() == InformationManager::Instance().getBasicSupplyProviderUnitType()) {
						isToEnqueue = false;
					}
				}
				if (isToEnqueue) {
					std::cout << "enqueue supply provider " << InformationManager::Instance().getBasicSupplyProviderUnitType().getName().c_str() << std::endl;
					BuildManager::Instance().buildQueue.queueAsHighestPriority(MetaType(InformationManager::Instance().getBasicSupplyProviderUnitType()), true);
				}
			}

		}
	}
}

//TODO : 언덕 진입 무빙 로직 필요
void StrategyManager::executeCombat()
{
	// 공격 모드가 아닐 때에는 전투유닛들을 아군 진영 길목에 집결시켜서 방어
	if (isFullScaleAttackStarted == false)		
	{
		BWTA::Chokepoint* firstChokePoint = InformationManager::Instance().getFirstChokePoint(InformationManager::Instance().selfPlayer);

		BWTA::Chokepoint* secondChokePoint = InformationManager::Instance().getSecondChokePoint(InformationManager::Instance().selfPlayer);

		for (auto & unit : BWAPI::Broodwar->self()->getUnits())
		{
			// TODO : 본진이 아닌곳에서 생산한 병력의 경우 해당 지역 방어에 사용해야함
			if (unit->getType() != InformationManager::Instance().getWorkerType(InformationManager::Instance().selfRace) && InformationManager::Instance().isCombatUnitType(unit->getType()) && unit->isIdle()) {
				if (InformationManager::Instance().getNumUnits(InformationManager::Instance().getBasicResourceDepotBuildingType(), BWAPI::Broodwar->self()) >= 2){
					CommandUtil::attackMove(unit, secondChokePoint->getCenter());
					//CommandUtil::attackMove(unit, InformationManager::Instance().getFirstExpansionLocation(InformationManager::Instance().selfPlayer)->getPosition());
				}
				else{
					bool checkTryFirstExtension = false;
					std::vector<ConstructionTask> * constructionQueue = ConstructionManager::Instance().getConstructionQueue();

					for (const auto & b : *constructionQueue)
					{
						std::string constructionState = "";

						if (b.status == ConstructionStatus::Unassigned)
						{
						}
						else if (b.status == ConstructionStatus::Assigned)
						{
							if (b.constructionWorker == nullptr) {
							}
							else {
								if (b.type == InformationManager::Instance().getBasicResourceDepotBuildingType()){
									checkTryFirstExtension = true;
								}
							}
						}
					}
					if (checkTryFirstExtension){
						CommandUtil::attackMove(unit, secondChokePoint->getCenter());
					}
					else{
						CommandUtil::attackMove(unit, firstChokePoint->getCenter());
					}
				}
			}
		}

		if (BWAPI::Broodwar->enemy()->getRace() == BWAPI::Races::Protoss){
			// 전투 유닛이 30개 이상 생산되었고, 적군 위치가 파악되었으면 총공격 모드로 전환
			if (BWAPI::Broodwar->self()->completedUnitCount(InformationManager::Instance().getBasicCombatUnitType()) + BWAPI::Broodwar->self()->completedUnitCount(InformationManager::Instance().getAdvancedCombatUnitType()) > 15) {

				if (InformationManager::Instance().enemyPlayer != nullptr
					&& InformationManager::Instance().enemyRace != BWAPI::Races::Unknown
					&& InformationManager::Instance().getOccupiedBaseLocations(InformationManager::Instance().enemyPlayer).size() > 0)
				{
					isFullScaleAttackStarted = true;
					printf(" ::::::::::::::::::::::::::: 총공격모드 ::::::::::::::::::::::::::: \n");
				}
			}
		}

		else if (BWAPI::Broodwar->enemy()->getRace() == BWAPI::Races::Terran){
			// 인구가 180이상 되었으면, 적군 위치가 파악되었으면 총공격 모드로 전환
			if (BWAPI::Broodwar->self()->supplyUsed()>360) {

				if (InformationManager::Instance().enemyPlayer != nullptr
					&& InformationManager::Instance().enemyRace != BWAPI::Races::Unknown
					&& InformationManager::Instance().getOccupiedBaseLocations(InformationManager::Instance().enemyPlayer).size() > 0)
				{
					isFullScaleAttackStarted = true;
					printf(" ::::::::::::::::::::::::::: 총공격모드 ::::::::::::::::::::::::::: \n");
				}
			}
		}

		else if (BWAPI::Broodwar->enemy()->getRace() == BWAPI::Races::Zerg){
			// 전투 유닛이 30개 이상 생산되었고, 적군 위치가 파악되었으면 총공격 모드로 전환
			if (BWAPI::Broodwar->self()->completedUnitCount(InformationManager::Instance().getBasicCombatUnitType()) + BWAPI::Broodwar->self()->completedUnitCount(InformationManager::Instance().getAdvancedCombatUnitType()) > 20) {

				if (InformationManager::Instance().enemyPlayer != nullptr
					&& InformationManager::Instance().enemyRace != BWAPI::Races::Unknown
					&& InformationManager::Instance().getOccupiedBaseLocations(InformationManager::Instance().enemyPlayer).size() > 0)
				{
					isFullScaleAttackStarted = true;
					printf(" ::::::::::::::::::::::::::: 총공격모드 ::::::::::::::::::::::::::: \n");
				}
			}
		}



	}
	// 공격 모드가 되면, 모든 전투유닛들을 적군 Main BaseLocation 로 공격 가도록 합니다
	else {
		//std::cout << "enemy OccupiedBaseLocations : " << InformationManager::Instance().getOccupiedBaseLocations(InformationManager::Instance().enemyPlayer).size() << std::endl;
		
		if (InformationManager::Instance().enemyPlayer != nullptr
			&& InformationManager::Instance().enemyRace != BWAPI::Races::Unknown
			&& InformationManager::Instance().getOccupiedBaseLocations(InformationManager::Instance().enemyPlayer).size() > 0)
		{
			// 공격 대상 지역 결정
			BWTA::BaseLocation * targetBaseLocation = nullptr;
			double closestDistance = 100000000;

			for (BWTA::BaseLocation * baseLocation : InformationManager::Instance().getOccupiedBaseLocations(InformationManager::Instance().enemyPlayer)) {

				double distance = BWTA::getGroundDistance(
					InformationManager::Instance().getMainBaseLocation(InformationManager::Instance().selfPlayer)->getTilePosition(), 
					baseLocation->getTilePosition());

				if (distance < closestDistance) {
					closestDistance = distance;
					targetBaseLocation = baseLocation;
				}
			}

			if (targetBaseLocation != nullptr) {

				for (auto & unit : BWAPI::Broodwar->self()->getUnits())
				{
					// 건물은 제외
					if (unit->getType().isBuilding()) continue;
					// 모든 일꾼은 제외
					if (unit->getType().isWorker()) continue;

					// canAttack 유닛은 attackMove Command 로 공격을 보냅니다
					if (unit->canAttack()) {

						if (unit->isIdle()) {
							CommandUtil::attackMove(unit, targetBaseLocation->getPosition());
						}
					}
				}
			}
		}
	}
}

void StrategyManager::executeProtossVsProtossBuildStrategy(){

	////////////////////////////////////////////////////////// 건물 관리
	for (auto & unit : BWAPI::Broodwar->self()->getUnits())
	{
		if (!unit->getType().isBuilding()){
			continue;
		}

		////////////////////////////////////////////////////////// 공통 업그레이크 관리
		if (InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Singularity_Charge) == 0 && unit->getType() == BWAPI::UnitTypes::Protoss_Cybernetics_Core && unit->isCompleted() && !unit->isUpgrading()){
			if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UpgradeTypes::Singularity_Charge) == 0){
				BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Singularity_Charge);
			}
		}
		if (InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Leg_Enhancements) == 0 && unit->getType() == BWAPI::UnitTypes::Protoss_Citadel_of_Adun && unit->isCompleted() && !unit->isUpgrading()){
			if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UpgradeTypes::Leg_Enhancements) == 0){
				BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Leg_Enhancements);
			}
		}
		// TODO : 스톰
		if (InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Leg_Enhancements) == 0 && unit->getType() == BWAPI::UnitTypes::Protoss_Citadel_of_Adun && unit->isCompleted() && !unit->isUpgrading()){
			if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UpgradeTypes::Leg_Enhancements) == 0){
				BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Leg_Enhancements);
			}
		}


		//옵저버 업그레이드 돈남아돌때 자원채취 3개이상일때만 하자
		//옵저버 시야업
		if (InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Nexus, BWAPI::Broodwar->self())>2){
			if (InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Sensor_Array) == 0 && unit->getType() == BWAPI::UnitTypes::Protoss_Observatory && unit->isCompleted() && !unit->isUpgrading()){
				if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UpgradeTypes::Sensor_Array) == 0){
					BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Sensor_Array);
				}
			}
			//옵저버 속업
			if (InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Sensor_Array) != 0 && InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Gravitic_Boosters) == 0 && unit->getType() == BWAPI::UnitTypes::Protoss_Observatory && unit->isCompleted() && !unit->isUpgrading()){
				if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UpgradeTypes::Gravitic_Boosters) == 0){
					BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Gravitic_Boosters);
				}
			}
		}

		if (unit->getType() == BWAPI::UnitTypes::Protoss_Forge && unit->isCompleted() && !unit->isUpgrading() && isTemplarArchivesComplete){
			int weapons = InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Protoss_Ground_Weapons);
			int armor = InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Protoss_Ground_Armor);
			int shields = InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Protoss_Plasma_Shields);
			if (weapons < 3){
				if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UpgradeTypes::Protoss_Ground_Weapons) == 0){
					BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Protoss_Ground_Weapons);
				}
			}
			else if (armor < 3){
				if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UpgradeTypes::Protoss_Ground_Armor) == 0){
					BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Protoss_Ground_Armor);
				}
				BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Protoss_Ground_Armor);
			}
			else if (shields < 3){
				if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UpgradeTypes::Protoss_Plasma_Shields) == 0){
					BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Protoss_Plasma_Shields);
				}

			}
		}

		//로보틱스에 대한 지시
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Robotics_Facility && unit->isIdle()) {
			//옵저버 3기까지는 무조건 생산 이후에는 인구수에따라 운용
			if (isObservatoryComplete && ((InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Observer, BWAPI::Broodwar->self()) < 3)
				|| (InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Observer, BWAPI::Broodwar->self()) < BWAPI::Broodwar->self()->supplyUsed() / 60 && BWAPI::Broodwar->self()->gas() > 75 && BWAPI::Broodwar->self()->minerals() > 25 && BWAPI::Broodwar->self()->supplyUsed() < 390))){
				if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Observer) == 0) {
					BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Observer);
				}
			}

			//셔틀은 상황에따라 1기 운용
			//if (InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Shuttle, BWAPI::Broodwar->self()) == 0 && BWAPI::Broodwar->self()->supplyUsed() < 390){
			//	if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Shuttle) == 0) {
			//		BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Shuttle);
			//	}
			//}
		}

		// 게이트웨이에 대한 지시
		if (unit->getType() == InformationManager::Instance().getBasicCombatBuildingType() && unit->isIdle()) {
			//질럿
			if (InformationManager::Instance().getNumUnits(InformationManager::Instance().getAdvancedCombatUnitType(), BWAPI::Broodwar->self())>3 && BWAPI::Broodwar->self()->minerals() > 150 && BWAPI::Broodwar->self()->supplyUsed() < 390) {
				if (unit->isTraining() == false || unit->getLarva().size() > 0) {
					if (BuildManager::Instance().buildQueue.getItemCount(InformationManager::Instance().getBasicCombatUnitType()) == 0) {
						BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicCombatUnitType());
					}
				}
			}
			//드라군
			if (isCyberneticsCoreComplete && BWAPI::Broodwar->self()->minerals() >= 125 && BWAPI::Broodwar->self()->gas() >= 50 && BWAPI::Broodwar->self()->supplyUsed() < 390) {
				if (unit->isTraining() == false || unit->getLarva().size() > 0) {
					if (BuildManager::Instance().buildQueue.getItemCount(InformationManager::Instance().getAdvancedCombatUnitType()) == 0) {
						BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getAdvancedCombatUnitType());
					}
				}
			}
			//다크템플러
			if (isTemplarArchivesComplete && BWAPI::Broodwar->self()->minerals() > 200 && BWAPI::Broodwar->self()->gas() > 200 && InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Dark_Templar, BWAPI::Broodwar->self()) < 4 && BWAPI::Broodwar->self()->supplyUsed() < 390) {
				if (unit->isTraining() == false || unit->getLarva().size() > 0) {
					if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Dark_Templar) == 0) {
						BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dark_Templar);
					}
				}
			}
			//하이템플러
			if (isTemplarArchivesComplete && BWAPI::Broodwar->self()->minerals() > 100 && BWAPI::Broodwar->self()->gas() > 300 && BWAPI::Broodwar->self()->supplyUsed() < 390) {
				if (unit->isTraining() == false || unit->getLarva().size() > 0) {
					if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_High_Templar) == 0) {
						BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_High_Templar);
					}
				}
			}
		}



		// 테크건물여부 체크
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Cybernetics_Core){
			if (unit->isCompleted()){
				isCyberneticsCoreComplete = true;
			}
			isCyberneticsCore = true;
		}
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Citadel_of_Adun){
			if (unit->isCompleted()){
				isCitadelOfAdunComplete = true;
			}
			isCitadelOfAdun = true;
		}
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Forge){
			if (unit->isCompleted()){
				isForgeComplete = true;
			}
			isForge = true;
		}
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Templar_Archives){
			if (unit->isCompleted()){
				isTemplarArchivesComplete = true;
			}
			isTemplarArchives = true;
		}
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Stargate){
			if (unit->isCompleted()){
				isStargateComplete = true;
			}
			isStargate = true;
		}
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Arbiter_Tribunal){
			if (unit->isCompleted()){
				isArbiterTribunalComplete = true;
			}
			isProtossArbiterTribunal = true;
		}
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Robotics_Facility){
			if (unit->isCompleted()){
				isRoboticsFacilityComplete = true;
			}
			isRoboticsFacility = true;
		}
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Observatory){
			if (unit->isCompleted()){
				isObservatoryComplete = true;
			}
			isObservatory = true;
		}
		/////////////////////////////////// 테크체크로직
	}

	////////////////////////////////////////////////////////// 테크(건물) 지시
	if (BWAPI::Broodwar->getFrameCount() % (24 * 5) == 0) {
		if (!isForge && BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Forge) == 0 && BWAPI::Broodwar->self()->supplyUsed() > 120){
			BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Forge, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
			isForge = true;
		}
		if (!isCyberneticsCore && BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Cybernetics_Core) == 0 && BWAPI::Broodwar->self()->supplyUsed() > 40){
			BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Cybernetics_Core, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
			isCyberneticsCore = true;
		}
		if (isCyberneticsCoreComplete){
			if (!isCitadelOfAdun && BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Citadel_of_Adun) == 0 && BWAPI::Broodwar->self()->supplyUsed() > 100 && BWAPI::Broodwar->self()->gas() > 100){
				BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Citadel_of_Adun, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
				isCitadelOfAdun = true;
			}
			if (!isRoboticsFacility && BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Robotics_Facility) == 0 && BWAPI::Broodwar->self()->supplyUsed() > 160 && BWAPI::Broodwar->self()->gas() > 200){
				BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Robotics_Facility, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
				isRoboticsFacility = true;
			}
		}
		if (isCitadelOfAdunComplete && !isTemplarArchives && BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Templar_Archives) == 0 && BWAPI::Broodwar->self()->supplyUsed() > 140 && BWAPI::Broodwar->self()->gas() > 200){
			BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Templar_Archives, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
			isTemplarArchives = true;
		}
		if (isRoboticsFacilityComplete && !isObservatory && BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Observatory) == 0 && BWAPI::Broodwar->self()->gas() > 100){
			BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Observatory, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
			isObservatory = true;
		}
	}

	// 자원관리 ,게이트갯수 10초에 한번만 실행
	if (BWAPI::Broodwar->getFrameCount() % (24 * 10) == 0) {
		////////////////////////////////////////////////////////// 게이트 갯수관리
		///TODO : 채취중인곳 기준
		if (BWAPI::Broodwar->self()->minerals() >= 300 && InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Gateway, InformationManager::Instance().selfPlayer) < 3 * InformationManager::Instance().getNumUnits(InformationManager::Instance().getBasicResourceDepotBuildingType(), BWAPI::Broodwar->self())){
			if (BuildManager::Instance().buildQueue.getItemCount(InformationManager::Instance().getBasicCombatBuildingType()) == 0) {
				if (InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Gateway, InformationManager::Instance().selfPlayer) < 8){
					BuildManager::Instance().buildQueue.queueAsHighestPriority(InformationManager::Instance().getBasicCombatBuildingType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
				}
				else{
					BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicCombatBuildingType(), BuildOrderItem::SeedPositionStrategy::MainBaseBackYard);
				}
			}
		}
		// 넥서스가 2개이상 or 게이트가 3개 이상이고, 가스가 2천 이하일 때 가스캐자
		if ((InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Nexus, InformationManager::Instance().selfPlayer) > 1 || InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Gateway, InformationManager::Instance().selfPlayer) > 2) && BWAPI::Broodwar->self()->gas() < 2000 && WorkerManager::Instance().getNumMineralWorkers() > 15){
			WorkerManager::Instance().turnOnGas();
		}
		// 가스가 4천이상일 경우 그만(1마리만)캐자
		if (BWAPI::Broodwar->self()->gas() > 4000){
			WorkerManager::Instance().turnOffGas();
		}
	}
}

void StrategyManager::executeProtossVsTerranBuildStrategy(){
	// InitialBuildOrder 진행중에는 아무것도 하지 않습니다
	if (isInitialBuildOrderFinished == false) {
		//예외 가스일꾼수조절
		//for (auto & unit : BWAPI::Broodwar->self()->getUnits())
		//{
		//	if (!unit->getType().isBuilding()){
		//		continue;
		//	}

		//	if (unit->getType() == BWAPI::UnitTypes::Protoss_Cybernetics_Core && unit->isUpgrading()){
		//		//처음 한번만 가스조절
		//		if (WorkerManager::Instance().isFirstTurnOffGas()){
		//			WorkerManager::Instance().turnOffGas();
		//			WorkerManager::Instance().firstTurnOffGasEnd();
		//		}
		//	}
		//}
		return;
	}


	////////////////////////////////////////////////////////// 건물 관리
	for (auto & unit : BWAPI::Broodwar->self()->getUnits())
	{
		if (!unit->getType().isBuilding()){
			continue;
		}

		////////////////////////////////////////////////////////// 공통 업그레이크 관리
		if (InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Singularity_Charge) == 0 && unit->getType() == BWAPI::UnitTypes::Protoss_Cybernetics_Core && unit->isCompleted() && !unit->isUpgrading()){
			if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UpgradeTypes::Singularity_Charge) == 0){
				BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Singularity_Charge);
			}
		}
		if (InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Leg_Enhancements) == 0 && unit->getType() == BWAPI::UnitTypes::Protoss_Citadel_of_Adun && unit->isCompleted() && !unit->isUpgrading()){
			if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UpgradeTypes::Leg_Enhancements) == 0){
				BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Leg_Enhancements);
			}
		}
		if (!InformationManager::Instance().selfPlayer->hasResearched(BWAPI::TechTypes::Recall) && unit->getType() == BWAPI::UnitTypes::Protoss_Arbiter_Tribunal && unit->isCompleted() && !unit->isUpgrading()){
			if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::TechTypes::Recall) == 0){
				BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::TechTypes::Recall);
			}
		}


		//옵저버 업그레이드 돈남아돌때 자원채취 3개이상일때만 하자
		//옵저버 시야업
		if (InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Nexus, BWAPI::Broodwar->self()) > 2){
			if (InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Sensor_Array) == 0 && unit->getType() == BWAPI::UnitTypes::Protoss_Observatory && unit->isCompleted() && !unit->isUpgrading()){
				if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UpgradeTypes::Sensor_Array) == 0){
					BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Sensor_Array);
				}
			}
			if (InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Sensor_Array) != 0 && InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Gravitic_Boosters) == 0 && unit->getType() == BWAPI::UnitTypes::Protoss_Observatory && unit->isCompleted() && !unit->isUpgrading()){
				if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UpgradeTypes::Gravitic_Boosters) == 0){
					BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Gravitic_Boosters);
				}
			}
		}

		if (unit->getType() == BWAPI::UnitTypes::Protoss_Forge && unit->isCompleted() && !unit->isUpgrading() && isTemplarArchives){
			int weapons = InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Protoss_Ground_Weapons);
			int armor = InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Protoss_Ground_Armor);
			int shields = InformationManager::Instance().selfPlayer->getUpgradeLevel(BWAPI::UpgradeTypes::Protoss_Plasma_Shields);
			if (weapons < 3){
				if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UpgradeTypes::Protoss_Ground_Weapons) == 0){
					BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Protoss_Ground_Weapons);
				}
			}
			else if (armor < 3){
				if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UpgradeTypes::Protoss_Ground_Armor) == 0){
					BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Protoss_Ground_Armor);
				}
				BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Protoss_Ground_Armor);
			}
			else if (shields < 3){
				if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UpgradeTypes::Protoss_Plasma_Shields) == 0){
					BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Protoss_Plasma_Shields);
				}

			}
		}

		//로보틱스에 대한 지시
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Robotics_Facility && unit->isIdle()) {
			//옵저버 3기까지는 무조건 생산 이후에는 인구수에따라 운용
			if (isObservatoryComplete && ((InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Observer, BWAPI::Broodwar->self()) < 3)
				|| (InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Observer, BWAPI::Broodwar->self()) < BWAPI::Broodwar->self()->supplyUsed() / 60 && BWAPI::Broodwar->self()->gas() > 75 && BWAPI::Broodwar->self()->minerals() > 25 && BWAPI::Broodwar->self()->supplyUsed() < 390))){
				if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Observer) == 0) {
					BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Observer);
				}
			}
			// TODO : 셔틀 상황에따라 2기까지 운용
			//else if (isObservatoryComplete && InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Shuttle, BWAPI::Broodwar->self()) == 0 && BWAPI::Broodwar->self()->supplyUsed() < 390){
			//	if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Shuttle) == 0) {
			//		BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Shuttle);
			//	}
			//}
		}

		// 게이트웨이에 대한 지시
		if (unit->getType() == InformationManager::Instance().getBasicCombatBuildingType() && unit->isIdle()) {
			//질럿
			if (InformationManager::Instance().getNumUnits(InformationManager::Instance().getAdvancedCombatUnitType(), BWAPI::Broodwar->self())>6 && BWAPI::Broodwar->self()->minerals() > 100 && BWAPI::Broodwar->self()->supplyUsed() < 390) {
				if (unit->isTraining() == false || unit->getLarva().size() > 0) {
					if (BuildManager::Instance().buildQueue.getItemCount(InformationManager::Instance().getBasicCombatUnitType()) == 0) {
						BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicCombatUnitType());
					}
				}
			}
			//드라군
			if (BWAPI::Broodwar->self()->minerals() >= 125 && BWAPI::Broodwar->self()->gas() >= 50 && BWAPI::Broodwar->self()->supplyUsed() < 390 && InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Cybernetics_Core, BWAPI::Broodwar->self()) != 0) {
				if (unit->isTraining() == false || unit->getLarva().size() > 0) {
					if (BuildManager::Instance().buildQueue.getItemCount(InformationManager::Instance().getAdvancedCombatUnitType()) == 0) {
						BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getAdvancedCombatUnitType());
					}
				}
			}
			//다크템플러
			if (BWAPI::Broodwar->self()->minerals() > 200 && BWAPI::Broodwar->self()->gas() > 200 && InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Templar_Archives, BWAPI::Broodwar->self()) != 0 && InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Dark_Templar, BWAPI::Broodwar->self()) < 10 && BWAPI::Broodwar->self()->supplyUsed() < 390) {
				if (unit->isTraining() == false || unit->getLarva().size() > 0) {
					if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Dark_Templar) == 0) {
						BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dark_Templar);
					}
				}
			}
		}
		//스타게이트에 대한 지시
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Stargate && unit->isIdle()) {
			//아비터
			if (isArbiterTribunalComplete && BWAPI::Broodwar->self()->gas() > 400 && BWAPI::Broodwar->self()->supplyUsed() < 390) {
				if (unit->isTraining() == false || unit->getLarva().size() > 0) {
					if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Arbiter) == 0) {
						BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Arbiter);
					}
				}
			}
		}


		// 테크건물여부 체크
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Cybernetics_Core){
			if (unit->isCompleted()){
				isCyberneticsCoreComplete = true;
			}
			isCyberneticsCore = true;
		}
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Citadel_of_Adun){
			if (unit->isCompleted()){
				isCitadelOfAdunComplete = true;
			}
			isCitadelOfAdun = true;
		}
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Forge){
			if (unit->isCompleted()){
				isForgeComplete = true;
			}
			isForge = true;
		}
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Templar_Archives){
			if (unit->isCompleted()){
				isTemplarArchivesComplete = true;
			}
			isTemplarArchives = true;
		}
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Stargate){
			if (unit->isCompleted()){
				isStargateComplete = true;
			}
			isStargate = true;
		}
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Arbiter_Tribunal){
			if (unit->isCompleted()){
				isArbiterTribunalComplete = true;
			}
			isProtossArbiterTribunal = true;
		}
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Robotics_Facility){
			if (unit->isCompleted()){
				isRoboticsFacilityComplete = true;
			}
			isRoboticsFacility = true;
		}
		if (unit->getType() == BWAPI::UnitTypes::Protoss_Observatory){
			if (unit->isCompleted()){
				isObservatoryComplete = true;
			}
			isObservatory = true;
		}
		/////////////////////////////////// 테크체크로직
	}

	////////////////////////////////////////////////////////// 테크(건물) 지시
	if (BWAPI::Broodwar->getFrameCount() % (24 * 5) == 0) {
		//if (!isForge && BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Forge) == 0 && BWAPI::Broodwar->self()->supplyUsed() > 120){
		//	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Forge, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
		//	// 템생산가스채취 대비
		//	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Assimilator, BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
		//	isForge = true;
		//}
		if (!isCyberneticsCore && BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Cybernetics_Core) == 0 && BWAPI::Broodwar->self()->supplyUsed() > 40){
			BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Cybernetics_Core, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
			isCyberneticsCore = true;
		}
		if (isCyberneticsCoreComplete){
			if (!isCitadelOfAdun && BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Citadel_of_Adun) == 0 && BWAPI::Broodwar->self()->supplyUsed() > 160 && BWAPI::Broodwar->self()->gas() > 100){
				BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Citadel_of_Adun, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
				isCitadelOfAdun = true;
			}
			//if (!isStargate && BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Stargate) == 0 && BWAPI::Broodwar->self()->supplyUsed() > 220 && BWAPI::Broodwar->self()->gas() > 150){
			//	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Stargate, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
			//	isStargate = true;
			//}
			if (!isRoboticsFacility && BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Robotics_Facility) == 0 && BWAPI::Broodwar->self()->supplyUsed() > 160 && BWAPI::Broodwar->self()->gas() > 200){
				BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Robotics_Facility, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
				isRoboticsFacility = true;
			}
		}
		//if (isCitadelOfAdunComplete && !isTemplarArchives && BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Templar_Archives) == 0 && BWAPI::Broodwar->self()->supplyUsed() > 140 && BWAPI::Broodwar->self()->gas() > 200){
		//	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Templar_Archives, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
		//	//혹시모를 벌처드랍/난입/레이스
		//	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Photon_Cannon, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
		//	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Photon_Cannon, BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
		//	isTemplarArchives = true;
		//}
		if (isStargateComplete && isCitadelOfAdunComplete && !isProtossArbiterTribunal && BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Arbiter_Tribunal) == 0 && BWAPI::Broodwar->self()->supplyUsed() > 240 && BWAPI::Broodwar->self()->gas() > 150){
			BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Arbiter_Tribunal, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
			isProtossArbiterTribunal = true;
		}
		if (isRoboticsFacilityComplete && !isObservatory && BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Observatory) == 0 && BWAPI::Broodwar->self()->gas() > 100){
			BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Observatory, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
			isObservatory = true;
		}
	}

	// 자원관리 ,게이트갯수 10초에 한번만 실행
	if (BWAPI::Broodwar->getFrameCount() % (24 * 10) == 0) {
		////////////////////////////////////////////////////////// 게이트 갯수관리
		///TODO : 채취중인곳 기준
		if (BWAPI::Broodwar->self()->minerals() >= 300 && InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Gateway, InformationManager::Instance().selfPlayer) < 3 * InformationManager::Instance().getNumUnits(InformationManager::Instance().getBasicResourceDepotBuildingType(), BWAPI::Broodwar->self())){
			if (BuildManager::Instance().buildQueue.getItemCount(InformationManager::Instance().getBasicCombatBuildingType()) == 0) {
				if (InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Gateway, InformationManager::Instance().selfPlayer) < 8){
					BuildManager::Instance().buildQueue.queueAsHighestPriority(InformationManager::Instance().getBasicCombatBuildingType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
				}
				else{
					BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicCombatBuildingType(), BuildOrderItem::SeedPositionStrategy::MainBaseBackYard);
				}
			}
		}
		// 게이트가 3개 이상이고, 가스가 2천 이하일 때 가스캐자
		// TODO 조건추가필요
		if (InformationManager::Instance().getNumUnits(BWAPI::UnitTypes::Protoss_Gateway, InformationManager::Instance().selfPlayer) > 2 && BWAPI::Broodwar->self()->gas() < 2000 && WorkerManager::Instance().getNumMineralWorkers() > 15){
			WorkerManager::Instance().turnOnGas();
		}
		// 가스가 3천이상일 경우 그만캐자
		// TODO 일꾼이 다죽었을경우 조건 추가
		if (BWAPI::Broodwar->self()->gas() > 3000){
			WorkerManager::Instance().turnOffGas();
		}
	}

}
void StrategyManager::executeProtossVsZergBuildStrategy(){


}


//void StrategyManager::executeTerranBuildStrategy(){
//	// InitialBuildOrder 진행중에는 아무것도 하지 않습니다
//	if (isInitialBuildOrderFinished == false) {
//		return;
//	}
//	// 5초에 한번만 실행
//	if (BWAPI::Broodwar->getFrameCount() % (24 * 5) != 0) {
//		return;
//	}
//
//	// 기본 병력 추가 훈련
//	if (BWAPI::Broodwar->self()->minerals() >= 50 && BWAPI::Broodwar->self()->supplyUsed() < 390) {
//		{
//			for (auto & unit : BWAPI::Broodwar->self()->getUnits())
//			{
//				if (unit->getType() == InformationManager::Instance().getBasicCombatBuildingType()) {
//					if (unit->isTraining() == false || unit->getLarva().size() > 0) {
//						if (BuildManager::Instance().buildQueue.getItemCount(InformationManager::Instance().getBasicCombatUnitType()) == 0) {
//							BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicCombatUnitType());
//						}
//					}
//				}
//			}
//		}
//	}
//}

//void StrategyManager::executeZergBuildStrategy(){
//	// InitialBuildOrder 진행중에는 아무것도 하지 않습니다
//	if (isInitialBuildOrderFinished == false) {
//		return;
//	}
//	// 5초에 한번만 실행
//	if (BWAPI::Broodwar->getFrameCount() % (24 * 5) != 0) {
//		return;
//	}
//
//	if (BWAPI::Broodwar->self()->minerals() >= 350){
//		printf("자봐봐300넘었다\n");
//		if (BuildManager::Instance().buildQueue.getItemCount(InformationManager::Instance().getBasicCombatBuildingType()) == 0) {
//			printf("해처리를 지을거야\n");
//			BuildManager::Instance().buildQueue.queueAsHighestPriority(InformationManager::Instance().getBasicCombatBuildingType());
//		}
//	}
//
//	// 기본 병력 추가 훈련
//	if (BWAPI::Broodwar->self()->minerals() >= 50 && BWAPI::Broodwar->self()->supplyUsed() < 390) {
//		{
//			for (auto & unit : BWAPI::Broodwar->self()->getUnits())
//			{
//				if (unit->getType() == InformationManager::Instance().getBasicCombatBuildingType()) {
//					if (unit->isTraining() == false || unit->getLarva().size() > 0) {
//						if (BuildManager::Instance().buildQueue.getItemCount(InformationManager::Instance().getBasicCombatUnitType()) == 0) {
//							BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicCombatUnitType());
//						}
//					}
//				}
//			}
//		}
//	}
//}

void StrategyManager::executeReconstruction(){
	////////////////////////////////////////////////////////////////////건물 파괴되었을 경우 복구로직
	//if (BWAPI::Broodwar->getFrameCount() % (24 * 10) == 0) {
	//	isCyberneticsCore = false;
	//	isCitadelOfAdun = false;
	//	isForge = false;
	//	isTemplarArchives = false;
	//	isStargate = false;
	//	isProtossArbiterTribunal = false;
	//	isRoboticsFacility = false;
	//	isObservatory = false;
	//}
}

// TODO: 로템 테란전일경우 따로 로직 필요 나머지는 동일
void StrategyManager::executeAnotherExtension(){
	// InitialBuildOrder 진행중에는 아무것도 하지 않습니다
	if (isInitialBuildOrderFinished == false) {
		return;
	}
	//게임시간기준 6분넘어갔을때부터 트라이
	//if (BWAPI::Broodwar->getFrameCount() < 10){
	if (BWAPI::Broodwar->getFrameCount() < 24 * 60 * 6 ){
		return;
	}
	
	// 임시 멀티 로직
	////////////////////////////////////////////////////////// 임시로직 인구65이상일때 30초에 한번 실행(체크)
	if (BWAPI::Broodwar->getFrameCount() % (24 * 30) == 0) {
		//////////////////////////////////////////////////////////////
		// 멀티 시도중인지 체크
		bool isNuxusing = false;
		for (auto & unit : BWAPI::Broodwar->self()->getUnits())
		{
			if (!unit->getType().isBuilding()){
				continue;
			}
			if (!unit->isCompleted() && unit->getType() == InformationManager::Instance().getBasicResourceDepotBuildingType()){
				isNuxusing = true;
			}
		}
		//멀티중인가? = 넥서스가 빌드큐에 있는가, 넥서스가 건설중인가
		if (isNuxusing || BuildManager::Instance().buildQueue.getItemCount(InformationManager::Instance().getBasicResourceDepotBuildingType())){
			InformationManager::Instance().resetTryExtensionLocation();
			return;
		}
		//////////////////////////////////////////////////////////////

		// 넥서스가 한개라면 앞마당부터 시도
		if (InformationManager::Instance().getNumUnits(InformationManager::Instance().getBasicResourceDepotBuildingType(), InformationManager::Instance().selfPlayer) == 1){
			if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Nexus) == 0 && ConstructionManager::Instance().getConstructionQueueItemCount(BWAPI::UnitTypes::Protoss_Nexus) == 0){
				BuildManager::Instance().buildQueue.queueAsHighestPriority(BWAPI::UnitTypes::Protoss_Nexus, BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation, true);
				BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Pylon, BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation, true);
			}
		}



		else if (BWAPI::Broodwar->self()->supplyUsed()>130){
			bool enemyCheck = false;
			for (BWTA::BaseLocation *  baseLocation: InformationManager::Instance().getOccupiedBaseLocations(InformationManager::Instance().enemyPlayer)){
				if (baseLocation == InformationManager::Instance().getTryExtensionLocation()){
					enemyCheck = true;
				}
			}

			// 멀티계획지역이 중립지역인가 체크해서 맞으면 계속 트라이하고 아니라면 타겟 변경
			if (enemyCheck || (InformationManager::Instance().getTryExtensionLocation() == nullptr && InformationManager::Instance().isNeutralBaseLocations(InformationManager::Instance().getNextExtensionBaseLocation())))
			{
				InformationManager::Instance().updateTryExtensionLocation();
				std::cout << "멀티를해볼까 제발 여기에 : " << InformationManager::Instance().getTryExtensionLocation()->getPosition() << std::endl;
			}
		}
	}
}


void StrategyManager::buildVsProtossObDra3Gate()
{
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//8파일런
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//10게이트
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//11가스
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Assimilator, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//13코어
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Cybernetics_Core, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Zealot);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//사업
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Singularity_Charge);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//로보
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Robotics_Facility);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//3게이트
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Observatory);
}

void StrategyManager::buildVsProtossFastDarkTemplar()
{
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//8파일런
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//10게이트
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//11가스
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Assimilator, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//13코어
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Cybernetics_Core, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Zealot);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//21로보
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Robotics_Facility);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//사업
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Singularity_Charge);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//3게이트
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
	//BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//3게이트
	//BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Templar_Archives);
	//BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	//BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Zealot);
	//BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	//BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dark_Templar);
	//BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dark_Templar);
	//BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
}

void StrategyManager::buildVsProtossDoubleNexus()
{
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//8파일런
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//10게이트
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//11가스
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Assimilator, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//13코어
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Cybernetics_Core, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Zealot);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//21로보
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Robotics_Facility);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//사업
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Singularity_Charge);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	//3게이트
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
}


void StrategyManager::buildVsTerranDoubleNexus()
{
	//더블넥 정석
	//(8파일런 10게이트 11가스 13코어 15파일런 17드라군 19사업 29넥서스 31로보)
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Assimilator, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Cybernetics_Core, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
	//19사업
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Singularity_Charge);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Nexus, BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Robotics_Facility);
}

void StrategyManager::buildVsTerranFastDarkTemplar()
{
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Assimilator, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Cybernetics_Core, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Zealot);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Singularity_Charge);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Nexus, BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
}

void StrategyManager::buildVsZergWeaponLegUpgradeZealot()
{
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Forge, BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Photon_Cannon, BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Nexus, BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Photon_Cannon, BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Assimilator, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
}

void StrategyManager::buildVsZergArchonFullAttackWithoutStargate()
{
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Forge, BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Photon_Cannon, BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Nexus, BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Photon_Cannon, BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::FirstExpansionLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Assimilator, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
}

void StrategyManager::buildVsRandom()
{
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getBasicSupplyProviderUnitType(), BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Zealot);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Assimilator, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(InformationManager::Instance().getWorkerType());
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Cybernetics_Core, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
	BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Zealot);
}

/*
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Assimilator, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);

BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Forge, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Photon_Cannon, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);

BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Gateway, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Zealot);

BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Cybernetics_Core, BuildOrderItem::SeedPositionStrategy::MainBaseLocation);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dragoon);
// 드라군 사정거리 업그레이드
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Singularity_Charge);

BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Citadel_of_Adun);
// 질럿 속도 업그레이드
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Leg_Enhancements);

BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Shield_Battery);

BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Templar_Archives);
// 하이템플러
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_High_Templar);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_High_Templar);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::TechTypes::Psionic_Storm);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::TechTypes::Hallucination);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Khaydarin_Amulet);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Archon);

// 다크아칸
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dark_Templar);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dark_Templar);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::TechTypes::Maelstrom);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::TechTypes::Mind_Control);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Argus_Talisman);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Dark_Archon);

BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Robotics_Facility);

// 셔틀
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Shuttle);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Robotics_Support_Bay);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Gravitic_Drive);

// 리버
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Reaver);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Scarab_Damage);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Reaver_Capacity);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Scarab);

BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Observatory);
// 옵저버
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Observer);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Gravitic_Boosters);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Sensor_Array);

// 공중유닛
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Stargate);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Fleet_Beacon);

// 스카우트
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Scout);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Apial_Sensors);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Gravitic_Thrusters);

// 커세어
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Corsair);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::TechTypes::Disruption_Web);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Argus_Jewel);

// 캐리어
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Carrier);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Carrier_Capacity);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Interceptor);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Interceptor);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Interceptor);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Interceptor);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Interceptor);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Interceptor);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Interceptor);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Interceptor);

// 아비터
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Arbiter_Tribunal);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UnitTypes::Protoss_Arbiter);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::TechTypes::Recall);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::TechTypes::Stasis_Field);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Khaydarin_Core);

// 포지 - 지상 유닛 업그레이드
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Protoss_Ground_Weapons);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Protoss_Plasma_Shields);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Protoss_Ground_Armor);

// 사이버네틱스코어 - 공중 유닛 업그레이드
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Protoss_Air_Weapons);
BuildManager::Instance().buildQueue.queueAsLowestPriority(BWAPI::UpgradeTypes::Protoss_Air_Armor);

*/