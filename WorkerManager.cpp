#include "WorkerManager.h"

using namespace MyBot;

WorkerManager::WorkerManager() 
{
	currentRepairWorker = nullptr;
	currentExtensionWorker = nullptr;
	firstTurnOffGas = true;
	rebalanceIsExpected = -1;
}

WorkerManager & WorkerManager::Instance() 
{
	static WorkerManager instance;
	return instance;
}

void WorkerManager::update() 
{
	if (BWAPI::Broodwar->getFrameCount() == rebalanceIsExpected){
		rebalanceWorkers();
	}

	// 1초에 1번만 실행한다
	if (BWAPI::Broodwar->getFrameCount() % 24 != 0) return;

	updateWorkerStatus();
	handleGasWorkers();
	handleIdleWorkers();
	handleMoveWorkers();
	handleCombatWorkers();
	handleRepairWorkers();

	handleDeffenceWorkers();

	if (BWAPI::Broodwar->getFrameCount() % (24 * 20) != 0) return;
	handleExtensionWorkers();

}

void WorkerManager::updateWorkerStatus() 
{
	// Drone 은 건설을 위해 isConstructing = true 상태로 건설장소까지 이동한 후, 
	// 잠깐 getBuildType() == none 가 되었다가, isConstructing = true, isMorphing = true 가 된 후, 건설을 시작한다

	// for each of our Workers
	for (auto & worker : workerData.getWorkers())
	{
		//if (workerData.getWorkerJob(worker) == WorkerData::Build && worker->getBuildType() == BWAPI::UnitTypes::None)
		//{
		//	std::cout << "construction worker " << worker->getID() << "buildtype BWAPI::UnitTypes::None " << std::endl;
		//}

		/*
		if (worker->isCarryingMinerals()) {
			std::cout << "mineral worker isCarryingMinerals " << worker->getID() 
				<< " isIdle: " << worker->isIdle()
				<< " isCompleted: " << worker->isCompleted()
				<< " isInterruptible: " << worker->isInterruptible()
				<< " target Name: " << worker->getTarget()->getType().getName()
				<< " job: " << workerData.getWorkerJob(worker)
				<< " exists " << worker->exists()
				<< " isConstructing " << worker->isConstructing()
				<< " isMorphing " << worker->isMorphing()
				<< " isMoving " << worker->isMoving()
				<< " isBeingConstructed " << worker->isBeingConstructed()
				<< " isStuck " << worker->isStuck()
				<< std::endl;
		}
		*/

		if (!worker->isCompleted())
		{
			continue;
		}

		// 게임상에서 worker가 isIdle 상태가 되었으면 (새로 탄생했거나, 그전 임무가 끝난 경우), WorkerData 도 Idle 로 맞춘 후, handleGasWorkers, handleIdleWorkers 등에서 새 임무를 지정한다 
		if ( worker->isIdle() )
		{
			/*
			if ((workerData.getWorkerJob(worker) == WorkerData::Build)
				|| (workerData.getWorkerJob(worker) == WorkerData::Move)
				|| (workerData.getWorkerJob(worker) == WorkerData::Scout)
				|| (workerData.getWorkerJob(worker) == WorkerData::Extension)) {

				std::cout << "idle worker " << worker->getID()
					<< " job: " << workerData.getWorkerJob(worker)
					<< " exists " << worker->exists()
					<< " isConstructing " << worker->isConstructing()
					<< " isMorphing " << worker->isMorphing()
					<< " isMoving " << worker->isMoving()
					<< " isBeingConstructed " << worker->isBeingConstructed()
					<< " isStuck " << worker->isStuck()
					<< std::endl;
			}
			*/

			// workerData 에서 Build / Move / Scout 로 임무지정한 경우, worker 는 즉 임무 수행 도중 (임무 완료 전) 에 일시적으로 isIdle 상태가 될 수 있다 
			if ((workerData.getWorkerJob(worker) != WorkerData::Build)
				&& (workerData.getWorkerJob(worker) != WorkerData::Move)
				&& (workerData.getWorkerJob(worker) != WorkerData::Scout)
				&& (workerData.getWorkerJob(worker) != WorkerData::Extension))
			{
				workerData.setWorkerJob(worker, WorkerData::Idle, nullptr);
			}
		}

		// if its job is gas
		if (workerData.getWorkerJob(worker) == WorkerData::Gas)
		{
			BWAPI::Unit refinery = workerData.getWorkerResource(worker);

			// if the refinery doesn't exist anymore (파괴되었을 경우)
			if (!refinery || !refinery->exists() ||	refinery->getHitPoints() <= 0)
			{
				workerData.setWorkerJob(worker, WorkerData::Idle, nullptr);
			}
		}

		// if its job is repair
		if (workerData.getWorkerJob(worker) == WorkerData::Repair)
		{
			BWAPI::Unit repairTargetUnit = workerData.getWorkerRepairUnit(worker);
						
			// 대상이 파괴되었거나, 수리가 다 끝난 경우
			if (!repairTargetUnit || !repairTargetUnit->exists() || repairTargetUnit->getHitPoints() <= 0 || repairTargetUnit->getHitPoints() == repairTargetUnit->getType().maxHitPoints())
			{
				workerData.setWorkerJob(worker, WorkerData::Idle, nullptr);
			}
		}
	}
}


void WorkerManager::handleGasWorkers()
{

	//// 9발업빌드일경우
	//if (BWAPI::Broodwar->self()->getRace() == BWAPI::Races::Zerg){

	//	if (Config::Macro::WorkersPerRefinery == 0){
	//		for (auto & worker : workerData.getWorkers())
	//		{
	//			// if its job is gas
	//			if (workerData.getWorkerJob(worker) == WorkerData::Gas && !worker->canReturnCargo())
	//			{
	//				printf("발업2 " << worker->getID() << std::endl;
	//				setMineralWorker(worker);
	//			}
	//		}
	//	}

	//	if (BWAPI::Broodwar->self()->gas() > 100 || BWAPI::Broodwar->self()->isUpgrading(BWAPI::UpgradeTypes::Metabolic_Boost)){
	//		Config::Macro::WorkersPerRefinery = 0;
	//		for (auto & worker : workerData.getWorkers())
	//		{
	//			// if its job is gas
	//			if (workerData.getWorkerJob(worker) == WorkerData::Gas && !worker->canReturnCargo())
	//			{
	//				printf("발업 " << worker->getID() << std::endl;
	//				workerData.setWorkerJob(worker, WorkerData::Idle, nullptr);
	//			}
	//		}
	//	}
	//	else if (BWAPI::Broodwar->self()->gas() > 90){
	//		Config::Macro::WorkersPerRefinery = 1;
	//		printf("90넘음" << Config::Macro::WorkersPerRefinery << std::endl;
	//		for (auto & worker : workerData.getWorkers())
	//		{
	//			// if its job is gas
	//			if (workerData.getWorkerJob(worker) == WorkerData::Gas && !worker->canReturnCargo())
	//			{
	//				printf("90 " << worker->getID() << std::endl;
	//				setMineralWorker(worker);
	//			}
	//		}
	//	}
	//	else if (BWAPI::Broodwar->self()->gas() > 80){
	//		Config::Macro::WorkersPerRefinery = 2;
	//		printf("80넘음" << Config::Macro::WorkersPerRefinery << std::endl;
	//		for (auto & worker : workerData.getWorkers())
	//		{
	//			// if its job is gas
	//			if (workerData.getWorkerJob(worker) == WorkerData::Gas && !worker->canReturnCargo())
	//			{
	//				printf("80 " << worker->getID() << std::endl;
	//				setMineralWorker(worker);
	//			}
	//		}
	//	}
	//}


	// for each unit we have
	for (auto & unit : BWAPI::Broodwar->self()->getUnits())
	{
		// refinery 가 건설 completed 되었으면,
		if (unit->getType().isRefinery() && unit->isCompleted() )
		{
			// get the number of workers currently assigned to it
			int numAssigned = workerData.getNumAssignedWorkers(unit);

			// if it's less than we want it to be, fill 'er up
			// 미네랄 일꾼은 적은데 가스 일꾼은 무조건 3~4명인 경우 -> Config::Macro::WorkersPerRefinery 값을 조정해야함
			for (int i = 0; i<(Config::Macro::WorkersPerRefinery - numAssigned); ++i)
			{
				BWAPI::Unit gasWorker = chooseGasWorkerFromMineralWorkers(unit);

				if (gasWorker)
				{
					//std::cout << "set gasWorker " << gasWorker->getID() << std::endl;
					workerData.setWorkerJob(gasWorker, WorkerData::Gas, unit);
				}
			}

			//if (Config::Macro::WorkersPerRefinery < 3){
			//	int i = 0;
			//	// for each of our workers
			//	for (auto & worker : workerData.getWorkers())
			//	{
			//		if (!worker) continue;
			//		if (workerData.getWorkerJob(worker) == WorkerData::Gas){
			//			if (i <= Config::Macro::WorkersPerRefinery) {
			//				//workerData.setWorkerJob(worker, WorkerData::Gas, unit);
			//				i++;
			//			}
			//			WorkerManager::Instance().setIdleWorker(worker);;
			//		}
			//	}
			//}
			//else{
			//	// get the number of workers currently assigned to it
			//	int numAssigned = workerData.getNumAssignedWorkers(unit);

			//	// if it's less than we want it to be, fill 'er up
			//	// 미네랄 일꾼은 적은데 가스 일꾼은 무조건 3~4명인 경우 -> Config::Macro::WorkersPerRefinery 값을 조정해야함
			//	for (int i = 0; i < (Config::Macro::WorkersPerRefinery - numAssigned); ++i)
			//	{
			//		BWAPI::Unit gasWorker = chooseGasWorkerFromMineralWorkers(unit);

			//		if (gasWorker)
			//		{
			//			//printf("set gasWorker " << gasWorker->getID() << std::endl;
			//			workerData.setWorkerJob(gasWorker, WorkerData::Gas, unit);
			//		}
			//	}
			//}
		}
	}
}

void WorkerManager::handleIdleWorkers() 
{
	// for each of our workers
	for (auto & worker : workerData.getWorkers())
	{
		if (!worker) continue;

		// if worker's job is idle 
		if (workerData.getWorkerJob(worker) == WorkerData::Idle || workerData.getWorkerJob(worker) == WorkerData::Default )
		{
			// send it to the nearest mineral patch
			setMineralWorker(worker);
		}
	}
}

void WorkerManager::handleMoveWorkers()
{
	// for each of our workers
	for (auto & worker : workerData.getWorkers())
	{
		if (!worker) continue;

		// if it is a move worker
		if (workerData.getWorkerJob(worker) == WorkerData::Move)
		{
			WorkerMoveData data = workerData.getWorkerMoveData(worker);

			// 목적지에 도착한 경우 이동 명령을 해제한다
			if (worker->getPosition().getDistance(data.position) < 4) {
				setIdleWorker(worker);
			}
			else {
				CommandUtil::move(worker, data.position);
			}
		}
	}
}



// bad micro for combat workers
void WorkerManager::handleCombatWorkers()
{
	for (auto & worker : workerData.getWorkers())
	{
		if (!worker) continue;

		if (workerData.getWorkerJob(worker) == WorkerData::Combat)
		{
			BWAPI::Broodwar->drawCircleMap(worker->getPosition().x, worker->getPosition().y, 4, BWAPI::Colors::Yellow, true);
			BWAPI::Unit target = getClosestEnemyUnitFromWorker(worker);

			if (target)
			{
				CommandUtil::attackUnit(worker, target);
			}
		}
	}
}

BWAPI::Unit WorkerManager::getClosestEnemyUnitFromWorker(BWAPI::Unit worker)
{
	if (!worker) return nullptr;

	BWAPI::Unit closestUnit = nullptr;
	double closestDist = 10000;

	for (auto & unit : BWAPI::Broodwar->enemy()->getUnits())
	{
		double dist = unit->getDistance(worker);

		if ((dist < 400) && (!closestUnit || (dist < closestDist)))
		{
			closestUnit = unit;
			closestDist = dist;
		}
	}

	return closestUnit;
}

void WorkerManager::stopCombat()
{
	for (auto & worker : workerData.getWorkers())
	{
		if (!worker) continue;

		if (workerData.getWorkerJob(worker) == WorkerData::Combat)
		{
			setMineralWorker(worker);
		}
	}
}

// TODO :: worker->getHitPoints()
void WorkerManager::handleDeffenceWorkers()
{
	return;
}

void WorkerManager::handleRepairWorkers()
{
	if (BWAPI::Broodwar->self()->getRace() != BWAPI::Races::Terran)
	{
		return;
	}

	for (auto & unit : BWAPI::Broodwar->self()->getUnits())
	{
		// 건물의 경우 아무리 멀어도 무조건 수리. 일꾼 한명이 순서대로 수리
		if (unit->getType().isBuilding() && unit->isCompleted() == true && unit->getHitPoints() < unit->getType().maxHitPoints())
		{
			BWAPI::Unit repairWorker = chooseRepairWorkerClosestTo(unit->getPosition());
			setRepairWorker(repairWorker, unit);
			break;
		}
		// 메카닉 유닛 (SCV, 시즈탱크, 레이쓰 등)의 경우 근처에 SCV가 있는 경우 수리. 일꾼 한명이 순서대로 수리
		else if (unit->getType().isMechanical() && unit->isCompleted() == true && unit->getHitPoints() < unit->getType().maxHitPoints())
		{
			// SCV 는 수리 대상에서 제외. 전투 유닛만 수리하도록 한다
			if (unit->getType() != BWAPI::UnitTypes::Terran_SCV) {
				BWAPI::Unit repairWorker = chooseRepairWorkerClosestTo(unit->getPosition(), 10 * TILE_SIZE);
				setRepairWorker(repairWorker, unit);
				break;
			}
		}

	}
}

BWAPI::Unit WorkerManager::chooseRepairWorkerClosestTo(BWAPI::Position p, int maxRange)
{
	if (!p.isValid()) return nullptr;

    BWAPI::Unit closestWorker = nullptr;
    double closestDist = 100000000;

	if (currentRepairWorker != nullptr && currentRepairWorker->exists() && currentRepairWorker->getHitPoints() > 0)
    {
		return currentRepairWorker;
    }

    // for each of our workers
	for (auto & worker : workerData.getWorkers())
	{
		if (!worker)
		{
			continue;
		}

		if (worker->isCompleted() 
			&& (workerData.getWorkerJob(worker) == WorkerData::Minerals || workerData.getWorkerJob(worker) == WorkerData::Idle || workerData.getWorkerJob(worker) == WorkerData::Move))
		{
			double dist = worker->getDistance(p);

			if (!closestWorker || dist < closestDist)
            {
				closestWorker = worker;
                dist = closestDist;
            }
		}
	}

	if (currentRepairWorker == nullptr || currentRepairWorker->exists() == false || currentRepairWorker->getHitPoints() <= 0) {
		currentRepairWorker = closestWorker;
	}

	return closestWorker;
}

void WorkerManager::handleExtensionWorkers()
{
	// 지금 확장안할건데 멀티용프로브가 지정되어있으면 해제하고 패스해
	if (InformationManager::Instance().getTryExtensionLocation() == nullptr)
	{
		if (currentExtensionWorker != nullptr){
			stopExtensioning(currentExtensionWorker);
		}
		return;
	}
	// 언능 확장해야 하는 상태면
	else{
		// 일꾼이 지정이 안되어있으면 하고
		BWAPI::Unit extensionWorker = chooseExtensionWorkerClosestTo(InformationManager::Instance().getTryExtensionLocation()->getPosition());
		// 멀티지역 도착했으면 쉬지말고 얼른건물지어
		if (currentExtensionWorker->isIdle() && extensionWorker->getPosition() == InformationManager::Instance().getTryExtensionLocation()->getPosition())
		{
			//파일런 빌드타임 = 20초에 한번실행/체크
			//우리땅이면 (파일런 지었으면)
			if (!InformationManager::Instance().isNeutralBaseLocations(InformationManager::Instance().getTryExtensionLocation())){
				//캐논과 넥서스 동시소환
				// 멀티 시도
				printf("넥서스 지어라\n");
				// TODO 현재 하드코딩 추후건물관리방식으로 변경필요 (삼룡이 이후부터 체크)
				if (BuildManager::Instance().buildQueue.getItemCount(BWAPI::UnitTypes::Protoss_Nexus) == 0 && ConstructionManager::Instance().getConstructionQueueItemCount(BWAPI::UnitTypes::Protoss_Nexus) == 0){
					BuildManager::Instance().buildQueue.queueAsHighestPriority(BWAPI::UnitTypes::Protoss_Nexus, InformationManager::Instance().getTryExtensionLocation()->getTilePosition(), true);
				}
			}
			//아니면
			else{
			//파일런부터짓고
				printf("파일런 지어라\n");
				if (BWAPI::Broodwar->self()->minerals()>=100){
					BuildManager::Instance().buildQueue.queueAsHighestPriority(BWAPI::UnitTypes::Protoss_Pylon, InformationManager::Instance().getTryExtensionLocation()->getTilePosition(), true);
				}
			}
		}
		// 건설중?
		else if (workerData.getJobCode(currentExtensionWorker) == 'B'){
			printf("건설중 기둘\n");
		}
		// 아직도 멀티하러 안갔으면 언넝가
		else
		{
			printf("빨리가자\n");
			setExtensionWorker(extensionWorker, InformationManager::Instance().getTryExtensionLocation()->getPosition());
		}
	}
}

BWAPI::Unit WorkerManager::chooseExtensionWorkerClosestTo(BWAPI::Position p, int maxRange)
{
	if (!p.isValid()) return nullptr;

	BWAPI::Unit closestWorker = nullptr;
	double closestDist = 100000000;

	if (currentExtensionWorker != nullptr && currentExtensionWorker->exists() && currentExtensionWorker->canMove())
	{
		return currentExtensionWorker;
	}

	// for each of our workers
	for (auto & worker : workerData.getWorkers())
	{
		if (!worker)
		{
			continue;
		}

		if (worker->isCompleted()
			&& (workerData.getWorkerJob(worker) == WorkerData::Minerals || workerData.getWorkerJob(worker) == WorkerData::Idle || workerData.getWorkerJob(worker) == WorkerData::Move))
		{
			double dist = worker->getDistance(p);

			if (!closestWorker || dist < closestDist)
			{
				closestWorker = worker;
				dist = closestDist;
			}
		}
	}

	if (currentExtensionWorker == nullptr || currentExtensionWorker->exists() == false || currentExtensionWorker->getHitPoints() <= 0) {
		currentExtensionWorker = closestWorker;
	}

	return closestWorker;
}

void WorkerManager::setExtensionWorker(BWAPI::Unit worker, BWAPI::Position positionToExtension){
	workerData.setWorkerJob(worker, WorkerData::Extension, nullptr);
	worker->move(positionToExtension);
}

void WorkerManager::stopExtensioning(BWAPI::Unit worker){
	workerData.setWorkerJob(worker, WorkerData::Idle, nullptr);
}

BWAPI::Unit WorkerManager::getScoutWorker()
{
    // for each of our workers
	for (auto & worker : workerData.getWorkers())
	{
		if (!worker)
		{
			continue;
		}
		// if it is a scout worker
        if (workerData.getWorkerJob(worker) == WorkerData::Scout) 
		{
			return worker;
		}
	}

    return nullptr;
}

// set a worker to mine minerals
void WorkerManager::setMineralWorker(BWAPI::Unit unit)
{
	if (!unit) return;

	// check if there is a mineral available to send the worker to
	BWAPI::Unit depot = getClosestResourceDepotFromWorker(unit);

	// if there is a valid ResourceDepot (Command Center, Nexus, Hatchery)
	if (depot)
	{
		// update workerData with the new job
		workerData.setWorkerJob(unit, WorkerData::Minerals, depot);
	}
}
BWAPI::Unit WorkerManager::getClosestMineralWorkerTo(BWAPI::Position target)
{
	BWAPI::Unit closestUnit = nullptr;
	double closestDist = 100000;
	
	for (auto & unit : BWAPI::Broodwar->self()->getUnits())
	{
		if (!unit)
		{
			continue;
		}

		if (unit->isCompleted()
			&& unit->getHitPoints() > 0
			&& unit->exists()
			&& unit->getType().isWorker()
			&& WorkerManager::Instance().isMineralWorker(unit))
		{
			double dist = unit->getDistance(target);
			if (!closestUnit || dist < closestDist)
			{
				closestUnit = unit;
				closestDist = dist;
			}
		}
	}

	return closestUnit;
}

BWAPI::Unit WorkerManager::getClosestResourceDepotFromWorker(BWAPI::Unit worker)
{
	if (!worker) return nullptr;

	BWAPI::Unit closestDepot = nullptr;
	double closestDistance = 0;

	for (auto & unit : BWAPI::Broodwar->self()->getUnits())
	{
		if (!unit) continue;
		
		// 가장 가까운, 일꾼수가 꽉 차지않은, 완성된 ResourceDepot (혹은 Lair 나 Hive로 변형중인 건물)을 찾는다
		if (unit->getType().isResourceDepot()
			&& (unit->isCompleted() || unit->getType() == BWAPI::UnitTypes::Zerg_Lair || unit->getType() == BWAPI::UnitTypes::Zerg_Hive) )
		{
			double distance = unit->getDistance(worker);

			// 일단 여러 ResourceDepot 중 하나는 선택되도록 한다
			if (!closestDepot )
			{
				closestDepot = unit;
				closestDistance = distance;
			}
			// 더 가까운 ResourceDepot 이 있고, 일꾼 수가 꽉 차지 않았다면 거기 가도록 한다
			else if (distance < closestDistance
				&& workerData.depotHasEnoughMineralWorkers(unit) == false) 
			{
				closestDepot = unit;
				closestDistance = distance;
			}
		}
	}

	return closestDepot;
}


// other managers that need workers call this when they're done with a unit
void WorkerManager::setIdleWorker(BWAPI::Unit unit)
{
	if (!unit) return;

	workerData.setWorkerJob(unit, WorkerData::Idle, nullptr);
}

// 해당 refinery 로부터 가장 가까운, Mineral 캐고있던 일꾼을 리턴한다
BWAPI::Unit WorkerManager::chooseGasWorkerFromMineralWorkers(BWAPI::Unit refinery)
{
	if (!refinery) return nullptr;

	BWAPI::Unit closestWorker = nullptr;
	double closestDistance = 0;

	for (auto & unit : workerData.getWorkers())
	{
		if (!unit) continue;

		if (unit->isCarryingMinerals()) continue;
		
		if (unit->isCompleted() && (workerData.getWorkerJob(unit) == WorkerData::Minerals))
		{
			double distance = unit->getDistance(refinery);
			if (!closestWorker || distance < closestDistance)
			{
				closestWorker = unit;
				closestDistance = distance;
			}
		}
	}

	return closestWorker;
}

void WorkerManager::setConstructionWorker(BWAPI::Unit worker, BWAPI::UnitType buildingType)
{
	if (!worker) return;

	workerData.setWorkerJob(worker, WorkerData::Build, buildingType);
}

BWAPI::Unit WorkerManager::chooseConstuctionWorkerClosestTo(BWAPI::UnitType buildingType, BWAPI::TilePosition buildingPosition, bool setJobAsConstructionWorker, int avoidWorkerID)
{
	// variables to hold the closest worker of each type to the building
	BWAPI::Unit closestMovingWorker = nullptr;
	BWAPI::Unit closestMiningWorker = nullptr;
	double closestMovingWorkerDistance = 0;
	double closestMiningWorkerDistance = 0;

	if (InformationManager::Instance().getTryExtensionLocation() != nullptr && buildingPosition == InformationManager::Instance().getTryExtensionLocation()->getTilePosition() && currentExtensionWorker!=nullptr){
		return currentExtensionWorker;
	}

	// look through each worker that had moved there first
	for (auto & unit : workerData.getWorkers())
	{
		if (!unit) continue;

		if (unit->isCarryingGas()) continue;

		// worker 가 2개 이상이면, avoidWorkerID 는 피한다
		if (workerData.getWorkers().size() >= 2 && avoidWorkerID != 0 && unit->getID() == avoidWorkerID) continue;

		// Move / Idle Worker
		if (unit->isCompleted() && (workerData.getWorkerJob(unit) == WorkerData::Move || workerData.getWorkerJob(unit) == WorkerData::Idle))
		{
			// if it is a new closest distance, set the pointer
			double distance = unit->getDistance(BWAPI::Position(buildingPosition));
			if (!closestMovingWorker || distance < closestMovingWorkerDistance)
			{
				if (BWTA::isConnected(unit->getTilePosition(), buildingPosition)) {
					closestMovingWorker = unit;
					closestMovingWorkerDistance = distance;
				}
			}
		}

		// Move / Idle Worker 가 없을때, 다른 Worker 중에서 차출한다 
		if (unit->isCompleted() && workerData.getWorkerJob(unit) != WorkerData::Move && workerData.getWorkerJob(unit) != WorkerData::Idle && workerData.getWorkerJob(unit) != WorkerData::Build)
		{
			// if it is a new closest distance, set the pointer
			double distance = unit->getDistance(BWAPI::Position(buildingPosition));
			if (!closestMiningWorker || distance < closestMiningWorkerDistance)
			{
				if (BWTA::isConnected(unit->getTilePosition(), buildingPosition)) {
					closestMiningWorker = unit;
					closestMiningWorkerDistance = distance;
				}
			}
		}
	}
	
	/*
	if (closestMiningWorker)
		std::cout << "closestMiningWorker " << closestMiningWorker->getID() << std::endl;
	if (closestMovingWorker)
		std::cout << "closestMovingWorker " << closestMovingWorker->getID() << std::endl;
	*/
	
	BWAPI::Unit chosenWorker = closestMovingWorker ? closestMovingWorker : (closestMiningWorker ? closestMiningWorker : currentExtensionWorker);

	// if the worker exists (one may not have been found in rare cases)
	if (chosenWorker && setJobAsConstructionWorker)
	{
		workerData.setWorkerJob(chosenWorker, WorkerData::Build, buildingType);
	}

	return chosenWorker;
}

// sets a worker as a scout
void WorkerManager::setScoutWorker(BWAPI::Unit worker)
{
	if (!worker) return;

	workerData.setWorkerJob(worker, WorkerData::Scout, nullptr);
}

// get a worker which will move to a current location
BWAPI::Unit WorkerManager::chooseMoveWorkerClosestTo(BWAPI::Position p)
{
	// set up the pointer
	BWAPI::Unit closestWorker = nullptr;
	double closestDistance = 0;

	// for each worker we currently have
	for (auto & unit : workerData.getWorkers())
	{
		if (!unit) continue;

		// only consider it if it's a mineral worker
		if (unit->isCompleted() && (workerData.getWorkerJob(unit) == WorkerData::Minerals || workerData.getWorkerJob(unit) == WorkerData::Idle))
		{
			// if it is a new closest distance, set the pointer
			double distance = unit->getDistance(p);
			if (!closestWorker || distance < closestDistance)
			{
				closestWorker = unit;
				closestDistance = distance;
			}
		}
	}

	// return the worker
	return closestWorker;
}

// sets a worker to move to a given location
void WorkerManager::setMoveWorker(BWAPI::Unit worker, int mineralsNeeded, int gasNeeded, BWAPI::Position p)
{
	// set up the pointer
	BWAPI::Unit closestWorker = nullptr;
	double closestDistance = 0;

	// for each worker we currently have
	for (auto & unit : workerData.getWorkers())
	{
		if (!unit) continue;
		
		// only consider it if it's a mineral worker or idle worker
		if (unit->isCompleted() && (workerData.getWorkerJob(unit) == WorkerData::Minerals || workerData.getWorkerJob(unit) == WorkerData::Idle))
		{
			// if it is a new closest distance, set the pointer
			double distance = unit->getDistance(p);
			if (!closestWorker || distance < closestDistance)
			{
				closestWorker = unit;
				closestDistance = distance;
			}
		}
	}

	if (closestWorker)
	{
		workerData.setWorkerJob(closestWorker, WorkerData::Move, WorkerMoveData(mineralsNeeded, gasNeeded, p));
	}
	else
	{
		//BWAPI::Broodwar->printf("Error, no worker found");
	}
}

void WorkerManager::setCombatWorker(BWAPI::Unit worker)
{
	if (!worker) return;

	workerData.setWorkerJob(worker, WorkerData::Combat, nullptr);
}

void WorkerManager::setRepairWorker(BWAPI::Unit worker, BWAPI::Unit unitToRepair)
{
	workerData.setWorkerJob(worker, WorkerData::Repair, unitToRepair);
}

void WorkerManager::stopRepairing(BWAPI::Unit worker)
{
	workerData.setWorkerJob(worker, WorkerData::Idle, nullptr);
}


void WorkerManager::onUnitMorph(BWAPI::Unit unit)
{
	if (!unit) return;

	// if something morphs into a worker, add it
	if (unit->getType().isWorker() && unit->getPlayer() == BWAPI::Broodwar->self() && unit->getHitPoints() >= 0)
	{
		workerData.addWorker(unit);
	}

	// if something morphs into a building, it was a worker (Zerg Drone)
	if (unit->getType().isBuilding() && unit->getPlayer() == BWAPI::Broodwar->self() && unit->getPlayer()->getRace() == BWAPI::Races::Zerg)
	{
		// 해당 worker 를 workerData 에서 삭제한다
		workerData.workerDestroyed(unit);
	}
}

void WorkerManager::onUnitShow(BWAPI::Unit unit)
{
	if (!unit) return;

	// add the depot if it exists
	if (unit->getType().isResourceDepot() && unit->getPlayer() == BWAPI::Broodwar->self())
	{
		workerData.addDepot(unit);
	}

	// add the worker
	if (unit->getType().isWorker() && unit->getPlayer() == BWAPI::Broodwar->self() && unit->getHitPoints() >= 0)
	{
		workerData.addWorker(unit);
	}
}

void WorkerManager::onUnitComplete(BWAPI::Unit unit)
{
	if (!unit) return;

	// TODO 완성되는 시점으로 변경필요
	if (unit->getType().isResourceDepot() && unit->getPlayer() == BWAPI::Broodwar->self())
	{
		printf("언제 리발란싱 되는지좀 볼까?\n");
		rebalanceIsExpected = BWAPI::Broodwar->getFrameCount() + 24;
	}
}

// 일하고있는 resource depot 에 충분한 수의 mineral worker 들이 지정되어 있다면, idle 상태로 만든다
// idle worker 에게 mineral job 을 부여할 때, mineral worker 가 부족한 resource depot 으로 이동하게 된다
//현재 미네랄 일꾼의 수
//현재 자원이 있고 채취가능한(최소테러당하지않고 완성된) 넥서스의 미네랄
//위 두 변수를 조합해서 알맞게 분배
// TODO 버그수정
// TODO 2
//테러(= 리버드랍, 러커드랍, 다크드랍, 핵공격 등)에 대한 개념정립 필요 - 테러시 일꾼 피난 로직
//테러해제시 원위치 필요 (rebalance)
void WorkerManager::rebalanceWorkers()
{
	int nMineralWorkers = workerData.getNumMineralWorkers();
	int totalMineral = 0;
	double wrokerPerMeneral = 0.0;
	std::list<BWAPI::Unit> avalibleDepotList = std::list<BWAPI::Unit>();

	//int assignedWorkers = getNumAssignedWorkers(depot);
	//int mineralsNearDepot = getMineralsNearDepot(depot);

	//미네랄을 갖고있는 모든 기지 확인
	for (auto & depot : workerData.getDepots())
	{
		printf("check ... \n");
		if (depot->isCompleted() && workerData.getMineralsNearDepot(depot) > 0){
			avalibleDepotList.push_back(depot);
			totalMineral += workerData.getMineralsNearDepot(depot);
			printf("confirm! %d\n", workerData.getMineralsNearDepot(depot));
		}
	}

	// 기지와 가까운 미네랄이 전부 바닥일 경우 종전의 알고리즘(맵전체 아무데나 막퍼오던) 적용
	if (totalMineral == 0){
		//모든 일꾼에 대하여
		for (auto & worker : workerData.getWorkers())
		{
			//현재 역할이 미네랄 채취가 아닌애들은 무시하고
			if (!workerData.getWorkerJob(worker) == WorkerData::Minerals)
			{
				continue;
			}

			// 현재 워커의 지정 넥서스
			BWAPI::Unit depot = workerData.getWorkerDepot(worker);

			//
			if (depot && workerData.depotHasEnoughMineralWorkers(depot))
			{
				workerData.setWorkerJob(worker, WorkerData::Idle, nullptr);
			}
			else if (!depot){
				workerData.setWorkerJob(worker, WorkerData::Idle, nullptr);
			}
		}
	}
	else{
		//미네랄당 일꾼수
		wrokerPerMeneral = (double) nMineralWorkers / (double) workerData.getNumMineralWorkers();

		printf("nMineralWorkers : %d\n", nMineralWorkers);
		printf("totalMineral : %d\n", totalMineral);
		printf("wrokerPerMeneral %d\n", wrokerPerMeneral);

		std::list<BWAPI::Unit> manyWorkersDepotList = std::list<BWAPI::Unit>();
		std::list<BWAPI::Unit> fewWorkersDepotList = std::list<BWAPI::Unit>();
		std::list<BWAPI::Unit> moveWorkerList = std::list<BWAPI::Unit>();
		//모든 기지에 대해 현재 상황파악
		for (auto & depot : workerData.getDepots())
		{
			int nOptimialWorker = wrokerPerMeneral * workerData.getMineralsNearDepot(depot);
			// 해당기지의 현재 일꾼 상태에 따라
			// 현재 일하고있는 일꾼수 = 미네랄에 따른 적정일꾼수일때는 건들지말고
			if (nOptimialWorker == workerData.getNumAssignedWorkers(depot)){
				avalibleDepotList.remove(depot);
				continue;
			}
			//현재 일하고있는 일꾼수 > 적정일꾼수
			else if (workerData.getNumAssignedWorkers(depot) > nOptimialWorker){
				manyWorkersDepotList.push_back(depot);
			}
			//현재 일하고있는 일꾼수 < 적정일꾼수
			if (workerData.getNumAssignedWorkers(depot) < nOptimialWorker){
				fewWorkersDepotList.push_back(depot);
			}
		}

		//일꾼선정
		for (auto & depot : manyWorkersDepotList)
		{
			int nCount = 0;
			int nOptimialWorker = wrokerPerMeneral * workerData.getMineralsNearDepot(depot);

			//모든 일꾼에 대하여
			for (auto & worker : workerData.getWorkers())
			{
				//현재 역할이 미네랄 채취가 아닌애들은 무시하고
				if (!workerData.getWorkerJob(worker) == WorkerData::Minerals)
				{
					continue;
				}

				// 현재 워커의 지정 넥서스가 many넥서스면
				if (depot == workerData.getWorkerDepot(worker))
				{
					// 적정수는 채운다
					if (nCount < nOptimialWorker){
						nCount++;
					}
					//다채우고나면
					else{
						moveWorkerList.push_back(worker);
					}
				}
			}
		}

		//분배
		for (auto & depot : fewWorkersDepotList)
		{
			int nCount = workerData.getNumAssignedWorkers(depot);
			int nOptimialWorker = wrokerPerMeneral * workerData.getMineralsNearDepot(depot);
			for (auto & worker : moveWorkerList)
			{
				if (nCount > nOptimialWorker){
					break;
				}
				worker->gather(workerData.getMineralPatchesNearDepot(depot).getClosestUnit());
				workerData.setWorkerJob(worker, WorkerData::Minerals, depot);
				nCount++;
			}
		}
	}
}

void WorkerManager::onUnitDestroy(BWAPI::Unit unit) 
{
	if (!unit) return;

	if (unit->getType().isResourceDepot() && unit->getPlayer() == BWAPI::Broodwar->self())
	{
		workerData.removeDepot(unit);
	}

	if (unit->getType().isWorker() && unit->getPlayer() == BWAPI::Broodwar->self()) 
	{
		workerData.workerDestroyed(unit);
	}

	if (unit->getType() == BWAPI::UnitTypes::Resource_Mineral_Field)
	{
		printf("언제 리발란싱 되는지좀 볼까?\n");
		rebalanceWorkers();
	}
}

bool WorkerManager::isMineralWorker(BWAPI::Unit worker)
{
	if (!worker) return false;

	return workerData.getWorkerJob(worker) == WorkerData::Minerals || workerData.getWorkerJob(worker) == WorkerData::Idle;
}

bool WorkerManager::isScoutWorker(BWAPI::Unit worker)
{
	if (!worker) return false;

	return (workerData.getWorkerJob(worker) == WorkerData::Scout);
}

bool WorkerManager::isConstructionWorker(BWAPI::Unit worker)
{
	if (!worker) return false;

	return (workerData.getWorkerJob(worker) == WorkerData::Build);
}

int WorkerManager::getNumMineralWorkers() 
{
	return workerData.getNumMineralWorkers();	
}

int WorkerManager::getNumIdleWorkers() 
{
	return workerData.getNumIdleWorkers();	
}

int WorkerManager::getNumGasWorkers() 
{
	return workerData.getNumGasWorkers();
}

WorkerData  WorkerManager::getWorkerData()
{
	return workerData;
}

bool WorkerManager::isFirstTurnOffGas(){
	return firstTurnOffGas;
}

void WorkerManager::firstTurnOffGasEnd(){
	firstTurnOffGas = false;
}

void WorkerManager::turnOffGas(){
	printf("가스일꾼조절!!!\n");
	Config::Macro::WorkersPerRefinery = 1;
	for (auto & worker : workerData.getWorkers())
	{
		if (workerData.getWorkerJob(worker) == WorkerData::Gas){
			worker->stop();
			setIdleWorker(worker);
		}
	}
}
void WorkerManager::turnOnGas(){
	Config::Macro::WorkersPerRefinery = 3;
}